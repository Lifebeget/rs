init: rmvendor \
	up \
	composeri \
	clear \
	freshdb \
	passport

up:
	docker-compose -f docker-compose-local.yml up -d --build

down:
	docker-compose down --remove-orphans

composeri:
	docker exec -it rescript-php-fpm composer install

passport:
	docker exec -it rescript-php-fpm php artisan passport:install

phpbash:
	docker exec -it rescript-php-fpm bash

rmvendor: down
	rm -rf ./application/vendor

clear:
	docker exec -it rescript-php-fpm php artisan cache:clear && docker exec -it rescript-php-fpm php artisan route:cache && docker exec -it rescript-php-fpm php artisan view:clear

freshdb:
	docker exec -it rescript-php-fpm php artisan migrate:fresh