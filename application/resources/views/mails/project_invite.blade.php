<!DOCTYPE html>
<html lang="en">
<head>
	<meta content="text/html" charset="UTF-8">
	<title>Reskript</title>
	<style>
		html, body {
            margin: 0;
            padding: 0;
        }
	</style>
</head>

<body>
	<div style="display: none; line-height: 0; color: #ffffff;">
		Invite letter
	</div>

	<table border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="#FFFFFF" style="margin:0 auto;	padding:0; max-width: 600px; vertical-align:top; border-collapse:collapse;" align="center">
		<tr>
			<td colspan="3" style="height:20px;margin: 0;padding: 0;"></td>
		</tr>
		<tr>
			<td colspan="3" align="left" width="600" style="margin:0; padding:0 0 0 20px;">
				<span style="font-family:Montserrat,Verdana,Geneva,sans-serif; color:#00001b; font-size:25px !important; line-height:26px !important; mso-line-height-rule:exactly; -webkit-text-size-adjust:none; font-weight:500; letter-spacing:normal; text-decoration: none;">{{ $inviteData['entity_for_email'] }} shared with you: {{ $inviteData['entity_name'] ?? '' }}</span>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="height:25px;margin: 0;padding: 0;"></td>
		</tr>
		<tr>
			<td colspan="3" style="margin: 0; padding:0;">
				<table border="0" cellpadding="0" cellspacing="0" align="center" width="600" bgcolor="#FFFFFF" style="margin:0 auto; padding:0; max-width: 600px; vertical-align: top;">
					<tr>
						<td style="width:19px; margin: 0;padding: 0;">
						</td>
						<td align="left" style="width:45px; margin: 0; padding: 0;">
							<img src="{{ url('assets/email/user.png') }}" style="font-size:0; display: block;" alt="" border="0" width="45" height="45">
						</td>
						<td align="left" valign="middle" style="width:536px; margin: 0;padding: 0;">
							<span style="font-family:Montserrat,Verdana,Geneva,sans-serif; color:#00001b; font-size:13px !important; line-height:normal; mso-line-height-rule:exactly; -webkit-text-size-adjust:none; font-weight:500; letter-spacing:normal; text-decoration: none;" >&nbsp;&nbsp;{{ $inviteData['inviter_name'] ?? '' }} has invited you to edit the following {{ $inviteData['entity_for_email'] }}:</span>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="height:45px; margin:0; padding:0;"></td>
		</tr>
		<tr>
			<td colspan="3" style="margin: 0; padding:0;">
				<table border="0" cellpadding="0" cellspacing="0" align="center" width="600" bgcolor="#FFFFFF" style="margin:0 auto; padding:0; max-width: 600px; vertical-align: top;">
					<tr>
						<td style="width:17px; margin: 0;padding: 0;">
						</td>
						<td align="left" valign="center" style="width:73px; margin: 0; padding:0; font-family:Montserrat,Verdana,Geneva,sans-serif; color:#000000; font-size:13.7px; line-height:normal; mso-line-height-rule:exactly; font-weight:500; letter-spacing:normal; vertical-align:middle;">
							<img src="{{ url('assets/email/file.png') }}" style="font-size:0; display: block;" alt="" border="0" width="62" height="62">
						</td>
						<td style="width:510px; margin: 0;padding: 0;">
							<a href="{{ $inviteData['link'] }}" style="color:#000000; text-decoration:underline; font-family: Montserrat,Verdana,Geneva,sans-serif; font-size:21px; line-height: normal; font-weight: 500; -webkit-text-size-adjust:none;">
								{{ $inviteData['entity_name'] ?? '' }}
							</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="height:59px; margin:0; padding:0;"></td>
		</tr>
		<tr>
			<td style="width:23px; margin:0; padding: 0;"></td>
			<td align="center" valign="center" style="width:160px; height:33px; margin: 0; padding:0;background-color:#008CF5; border-radius: 4px;">
				<a href="{{ $inviteData['link'] }}" style=" font-family:Montserrat,Verdana,Geneva,sans-serif; color:#FFFFFF; font-size:12px; line-height:33px; mso-line-height-rule:exactly; -webkit-text-size-adjust:none; font-weight:500; letter-spacing:normal; vertical-align:middle; text-decoration: none;">
					Click here to view
				</a>
			</td>
			<td style="width:417px; margin:0; padding: 0;"></td>
		</tr>
		<tr>
			<td colspan="3" style="height:35px; margin:0; padding:0;"></td>
		</tr>
		<tr>
			<td colspan="3" align="left" valign="center" style="margin: 0; padding:0 0 0 23px; vertical-align:middle;">
				<img src="{{ url('assets/email/goog_logo.png') }}" style="padding-right:11px; font-size:0;" alt="" border="0" width="86">
			</td>
		</tr>
		<tr>
			<td colspan="3" align="left" valign="center" style="margin: 0; padding:2px 0 0 22px; vertical-align:middle; font-family:Montserrat,Verdana,Geneva,sans-serif; color:#707070; font-size:12.1px; line-height:15px; mso-line-height-rule:exactly; -webkit-text-size-adjust:none; font-weight:400; letter-spacing:normal;">
				You have received this email, because<br>
                 shared a {{ $inviteData['entity_for_email'] }}<br>
				with you from ReSkript<br>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="left" valign="center" style="margin: 0; padding:6px 0 0 22px; font-family:Montserrat,Verdana,Geneva,sans-serif; color:#707070; font-size:12.1px; line-height:15px; mso-line-height-rule:exactly; font-weight:400; letter-spacing:normal;">
				<a href="https://www.reskript.com/" style="font-family:Montserrat,Verdana,Geneva,sans-serif; color:#707070; font-size:12.1px; line-height:15px; mso-line-height-rule:exactly; -webkit-text-size-adjust:none; font-weight:400; letter-spacing:normal; text-decoration: underline;" target="_blank">Reskript.com</a> secured document<br>
				collaboration platform
			</td>
		</tr>
		<tr>
			<td colspan="3" style="height:80px; margin:0; padding:0;"></td>
		</tr>
		<tr>
			<td colspan="3" style="height:1px;margin: 0;padding: 0;">
				<img src="{{ url('assets/email/gmail-fix.jpg') }}" style="font-size:0; display:block;" alt="" border="0" width="600" height="1">
			</td>
		</tr>
	</table>
</body>
</html>
