<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Callback</title>
    <script>
        const message = {
            data: {
                token: {!! json_encode($token, JSON_HEX_TAG)  !!},
                code: parseInt(`{{ $code }}`)
            }
        };

        window.opener.postMessage(message, '*');
        window.close();
    </script>
</head>
<body>
</body>
</html>
