<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Callback</title>
    <script>
        const message = {
            data: {
                message: `{{ $message }}`,
                code: parseInt(`{{ $code }}`)
            }
        };

        window.opener.postMessage(message, '*');
        window.close();
    </script>
</head>
<body>
</body>
</html>
