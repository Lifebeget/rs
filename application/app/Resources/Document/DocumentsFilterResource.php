<?php

namespace App\Resources\Document;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DocumentsFilterResource extends JsonResource
{
    private $documents = ['my_documents' => [], 'shared_documents' => []];

    public function __construct($resource)
    {
        parent::__construct($resource);

        foreach ($resource as $item) {
            $document = [
                'id' => $item->document_id,
                'name' => $item->document->name,
                'image' => $item->document->image,
                'entity_id' => $item->document->entity_id,
                'entity_type' => $item->document->entity_type,
                'user_id' => $item->user_id,
                'settings' => $item->settings,
                'is_owner' => $item->is_owner,
                'is_bookmark' => $item->is_bookmark,
                'created_at' => $item->created_at,
                'updated_at' => $item->updated_at,
                'deleted_at' => $item->deleted_at,
            ];

            if ($item->is_owner) {
                $this->documents['my_documents'][] = $document;
            } else {
                $this->documents['shared_documents'][] = $document;
            }
        }
    }

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->documents;
    }
}
