<?php

namespace App\Resources\Document;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DocumentsFilterUsersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'email' => $this->email,
            'email_verified_at' => $this->email_verified_at,
            'color' => $this->color,
            'country' => $this->country,
            'blocked' => $this->blocked,
            'is_owner' => $this->documents[0]->is_owner,
            'is_bookmark' => $this->documents[0]->is_bookmark,
            'settings' => $this->documents[0]->settings,
            'status' => $this->documents[0]->status
        ];
    }
}
