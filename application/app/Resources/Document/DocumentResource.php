<?php

namespace App\Resources\Document;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'count' => $this->document_count,
            'result' =>  new DocumentsFilterResource($this->documents()->whereNull('deleted_at')->get())
        ];
    }
}
