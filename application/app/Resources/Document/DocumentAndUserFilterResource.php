<?php

namespace App\Resources\Document;

use App\Services\Breadcrumbs\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DocumentAndUserFilterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'entity_id' => $this->entity_id,
            'entity_type' => $this->entity_type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'parents' => Breadcrumbs::breadcrumbs($this),
        ];
        $userIsOwner = null;
        if ($this->users->toArray()) {
            foreach ($this->users->toArray() as $item) {
                if ($item['id'] == $request->user()->id) {
                    $userIsOwner = $item;
                    break;
                }
            }
        }
        if ($userIsOwner  && count($userIsOwner['documents']) && $userIsOwner['documents'][0]['is_owner']) {
            $data['is_owner'] = true;
            $data['users'] = DocumentsFilterUsersResource::collection($this->users);
        } elseif ($userIsOwner && count($userIsOwner['documents'])) {
            $data['settings'] = $userIsOwner['documents'][0]['settings'];
            $data['users'] = DocumentsFilterNonOwnerUsersResource::collection($this->users);
        }
        return $data;
    }
}
