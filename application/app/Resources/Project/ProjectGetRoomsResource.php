<?php

namespace App\Resources\Project;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectGetRoomsResource extends JsonResource
{
    private $roomLibrary = [];

    public function __construct($resource)
    {
        parent::__construct($resource);
        for ($i = 0; $i < count($resource['rooms']); $i++) {
            if (!$resource['rooms'][$i]['deleted_at']) {
                $this->roomLibrary[] = [
                    'id' => $resource['rooms'][$i]['id'],
                    'name' => $resource['rooms'][$i]['name'],
                    'image' => $resource['rooms'][$i]['image'],
                    'created_at' => $resource['rooms'][$i]['created_at'],
                    'updated_at' => $resource['rooms'][$i]['updated_at'],
                    'deleted_at' => $resource['rooms'][$i]['deleted_at'],
                    'settings' => $resource['rooms'][$i]['room'][0]['settings'] ?? [],
                    'is_owner' => $resource['rooms'][$i]['room'][0]['is_owner'] ?? [],
                    'is_bookmark' => $resource['rooms'][$i]['room'][0]['is_bookmark'] ?? [],
                    'users' => $resource['rooms'][$i]['room'][0]['users'] ?? []
                ];
            }
        }
    }

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->roomLibrary;
    }
}
