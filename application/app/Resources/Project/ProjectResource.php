<?php

namespace App\Resources\Project;

use App\Services\Breadcrumbs\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'description' => $this->description,
            'user_id' => $this->user_id,
            'icon' => $this->icon,
            'color' => $this->color,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'parents' => Breadcrumbs::breadcrumbs($this),
            'project_documents' => ProjectDocumentsResource::collection(
                $this->documents()->whereNull('deleted_at')->get()
            ) ?? [],
            'project_folders' => ProjectFoldersResource::collection(
                $this->folders()->whereNull('deleted_at')->get()
            ) ?? [],
            'project_rooms' => new ProjectRoomsResource([
                    'rooms' => $this->rooms
                ]) ?? [],
            'project_library' => new ProjectLibraryResource([
                    'documents' => $this->documents,
                    'folders' => $this->folders,
                    'rooms' => $this->rooms
                ]) ?? []
        ];
        $user = null;
        if ($this->users->toArray()) {
            foreach ($this->users->toArray() as $item) {
                if ($item['id'] == $request->user()->id) {
                    $user = $item;
                    break;
                }
            }
        }
        if ($user && count($user['user_projects']) && $user['user_projects'][0]['is_owner']) {
            $data['is_owner'] = true;
            $data['users'] = ProjectUsersFilterResource::collection($this->users);
        } elseif ($user && count($user['user_projects'])) {
            $data['settings'] = $user['user_projects'][0]['settings'];
            $data['users'] = ProjectNonOwnerUserResource::collection($this->users);
        }

        return $data;
    }
}
