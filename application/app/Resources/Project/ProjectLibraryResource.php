<?php

namespace App\Resources\Project;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectLibraryResource extends JsonResource
{
    private $projectLibrary = [];

    public function __construct($resource)
    {
        parent::__construct($resource);

        foreach ($resource['documents'] as $document) {
            if (!$document['deleted_at']) {
                $this->projectLibrary[] = [
                    'id' => $document['id'],
                    'name' => $document['name'],
                    'image' => $document['image'],
                    'entity_id' => $document['entity_id'],
                    'entity_type' => $document['entity_type'],
                    'created_at' => $document['created_at'],
                    'updated_at' => $document['updated_at'],
                    'deleted_at' => $document['deleted_at'],
                ];
            }
        }


        foreach ($resource['folders'] as $folder) {
            if (!$folder['deleted_at'] && count($folder->notDeletedDocuments->toArray())) {
                $this->projectLibrary = array_merge($this->projectLibrary, $folder->notDeletedDocuments->toArray());
            }
        }

        foreach ($resource['rooms'] as $room) {
            if (!$room['deleted_at'] && count($room->notDeletedDocuments->toArray())) {
                $this->projectLibrary = array_merge($this->projectLibrary, $room->notDeletedDocuments->toArray());
            }
            if ($room->folders && count($room->folders)) {
                foreach ($room->folders as $folder) {
                    if ($folder->notDeletedDocuments && count($folder->notDeletedDocuments)) {
                        $this->projectLibrary = array_merge(
                            $this->projectLibrary,
                            $folder->notDeletedDocuments->toArray()
                        );
                    }
                }
            }
        }
    }

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->projectLibrary;
    }
}
