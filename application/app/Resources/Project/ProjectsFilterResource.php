<?php

namespace App\Resources\Project;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectsFilterResource extends JsonResource
{
    private $projects = [];

    public function __construct($resources)
    {
        parent::__construct($resources);
        foreach ($resources as $resource) {
            if (isset($resource['project']) && !$resource['project']['deleted_at']) {
                $this->projects[] = [
                    'id' => $resource['project']['id'],
                    'name' => $resource['project']['name'],
                    'description' => $resource['project']['description'],
                    'image' => $resource['project']['image'],
                    'icon' => $resource['project']['icon'],
                    'is_owner' => $resource['is_owner'],
                    'is_bookmark' => $resource['is_bookmark'],
                    'user_id' => $resource['user_id'],
                    'color' => $resource['project']['color'],
                    'project_documents' => ProjectDocumentsResource::collection(
                        $resource['project']->documents()->whereNull('deleted_at')->get()
                    ) ?? [],
                    'project_folders' => ProjectFoldersResource::collection(
                        $resource['project']->folders()->whereNull('deleted_at')->get()
                    ) ?? [],
                    'project_rooms' => new ProjectGetRoomsResource(
                        $resource['project']->load('rooms.room.users')->toArray()
                    ) ?? [],
                    'project_library' => new ProjectLibraryResource([
                        'documents' => $resource['project']->documents,
                        'folders' => $resource['project']->folders,
                        'rooms' => $resource['project']->rooms
                    ]) ?? [],
                ];
            }
        }
    }

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->projects;
    }
}
