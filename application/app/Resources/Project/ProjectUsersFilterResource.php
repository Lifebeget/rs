<?php

namespace App\Resources\Project;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class ProjectUsersFilterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'surname' => $this->surname,
            'image' => $this->image,
            'email' => $this->email,
            'email_verified_at' => $this->email_verified_at,
            'color' => $this->color,
            'country' => $this->country,
            'settings' => $this->userProjects[0]->settings,
            'blocked' => $this->blocked,
            'is_owner' => $this->userProjects[0]->is_owner,
            'is_bookmark' => $this->userProjects[0]->is_bookmark,
            'role_name' => $this->userProjects[0]->role_name
        ];
    }
}
