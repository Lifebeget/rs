<?php

namespace App\Resources\Folder;

use App\Services\Breadcrumbs\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FolderAndUserFilterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'entity_id' => $this->entity_id,
            'entity_type' => $this->entity_type,
            'user_id' => $this->folder->user_id,
            'settings' => $this->folder->settings,
            'is_owner' => $this->folder->is_owner,
            'is_bookmark' => $this->folder->is_bookmark,
            'users' => $this->folder->users,
            'folder_documents' => $this->notDeletedDocuments,
            'folder_childFolders' =>$this->notDeletedChildFolders,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            "parents" => Breadcrumbs::breadcrumbs($this),
        ];
    }
}
