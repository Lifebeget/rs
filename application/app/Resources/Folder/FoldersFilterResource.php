<?php

namespace App\Resources\Folder;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FoldersFilterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (!$this->folder->entity_type) {
            return [
                'id' => $this->folder_id,
                'name' => $this->folder->name,
                'image' => $this->folder->image,
                'entity_id' => $this->folder->entity_id,
                'entity_type' => $this->folder->entity_type,
                'user_id' => $this->user_id,
                'settings' => $this->settings,
                'is_owner' => $this->is_owner,
                'is_bookmark' => $this->is_bookmark,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                'deleted_at' => $this->deleted_at,
            ];
        }
    }
}
