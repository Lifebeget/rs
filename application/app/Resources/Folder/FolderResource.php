<?php

namespace App\Resources\Folder;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FolderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'count' => $this->folder_count,
            'result' => FoldersFilterResource::collection($this->folders()->whereNull('deleted_at')->get())
        ];
    }
}
