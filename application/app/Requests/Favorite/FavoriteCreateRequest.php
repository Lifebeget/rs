<?php

namespace App\Requests\Favorite;

use App\Requests\AbstractFormRequest;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

class FavoriteCreateRequest extends AbstractFormRequest
{
    /**
     * @return bool|Authenticatable|null
     */
    public function authorize()
    {
        return Auth::user() ?? true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'exists:users,id',
            'entity_type' => 'required',
            'entity_id' => 'required'
        ];
    }
}
