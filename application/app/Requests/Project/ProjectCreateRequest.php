<?php

namespace App\Requests\Project;

use App\Requests\AbstractFormRequest;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

class ProjectCreateRequest extends AbstractFormRequest
{
    /**
     * @return bool|Authenticatable|null
     */
    public function authorize()
    {
        return Auth::user() ?? true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:projects,name',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:256',
            'color' => 'nullable|string|starts_with:#'
        ];
    }
}
