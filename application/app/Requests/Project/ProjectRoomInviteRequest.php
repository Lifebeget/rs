<?php

namespace App\Requests\Project;

use App\Requests\AbstractFormRequest;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

class ProjectRoomInviteRequest extends AbstractFormRequest
{
    /**
     * @return bool|Authenticatable|null
     */
    public function authorize()
    {
        return Auth::user() ?? true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'required',
            'email' => 'required',
            'entity_id' => 'required|exists:rooms,id',
            'entity_type' => 'required',
            'settings' => 'required',
            'inviter_name' => 'required'
        ];
    }
}
