<?php

namespace App\Requests\Document;

use App\Requests\AbstractFormRequest;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

class DocumentInviteRequest extends AbstractFormRequest
{
    /**
     * @return bool|Authenticatable|null
     */
    public function authorize()
    {
        return Auth::user() ?? true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'inviter_name' => 'required',
            'email' => 'required',
            'entity_id' => 'required|exists:documents,id',
            'entity_name' => 'required',
            'settings' => 'required'
        ];
    }
}
