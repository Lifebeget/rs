<?php

namespace App\Requests\Folder;

use App\Requests\AbstractFormRequest;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

class FolderInviteRequest extends AbstractFormRequest
{
    /**
     * @return bool|Authenticatable|null
     */
    public function authorize()
    {
        return Auth::user() ?? true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'inviter_name' => 'required',
            'email' => 'required',
            'entity_id' => 'required|exists:folders,id',
            'entity_name' => 'required',
            'settings' => 'required'
        ];
    }
}
