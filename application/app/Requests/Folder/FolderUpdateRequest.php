<?php

namespace App\Requests\Folder;

use App\Requests\AbstractFormRequest;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

class FolderUpdateRequest extends AbstractFormRequest
{
    /**
     * @return bool|Authenticatable|null
     */
    public function authorize()
    {
        return Auth::user() ?? true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:256',
            'project_id' => 'exists:projects,id',
        ];
    }
}
