<?php

namespace App\Repositories\Project;

use App\Entity\ProjectPackages\Project;
use App\Repositories\AbstractRepository;
use App\Repositories\Document\DocumentRepository;
use App\Repositories\Folder\FolderRepository;
use App\Repositories\Room\RoomRepository;
use App\Repositories\User\UserProjectRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ProjectRepository extends AbstractRepository
{
    private UserProjectRepository $userProjectRepository;
    private RoomRepository $roomRepository;
    private FolderRepository $folderRepository;
    private DocumentRepository $documentRepository;

    /**
     * ProjectRepository constructor.
     * @param UserProjectRepository $userProjectRepository
     *  * @param RoomRepository $roomRepository
     * @param FolderRepository $folderRepository
     * @param DocumentRepository $documentRepository
     */
    public function __construct(
        UserProjectRepository $userProjectRepository,
        RoomRepository        $roomRepository,
        FolderRepository      $folderRepository,
        DocumentRepository    $documentRepository
    )
    {
        $this->model = new Project();
        $this->userProjectRepository = $userProjectRepository;
        $this->roomRepository = $roomRepository;
        $this->folderRepository = $folderRepository;
        $this->documentRepository = $documentRepository;
    }

    /**
     * @param array $projectId
     * @return bool
     * @throws FileNotFoundException
     */
    public function deleteManyProjects(array $projectId): bool
    {
        $cachedProjects = null;

        if (Storage::disk('public')->exists('project.txt')) {
            $cachedProjects = json_decode(Storage::disk('public')->get('project.txt'), true);
        }

        $projects = $this
            ->model
            ->whereIn('id', $projectId);

        foreach ($projects->get() as $project) {
            $key = array_search($project->id, $cachedProjects ?? []);

            if (false !== $key) {
                unset($cachedProjects[$key]);
                if (!$cachedProjects) {
                    Storage::disk('public')->delete('project.txt');
                } else {
                    Storage::disk('public')->put('project.txt', json_encode($cachedProjects));
                }
            }

            $room = $project->rooms()->get();
            if (count($room)) {
                $this->roomRepository->deleteManyRooms('', $room);
            }

            $folders = $project->folders()->get();
            if (count($folders)) {
                $this->folderRepository->deleteManyFolders('', $folders);
            }
            $documents = $project->documents()->get();
            $this->documentRepository->deleteManyDocuments('', $documents);
        }
        $this->userProjectRepository
            ->newQuery()
            ->whereIn('project_id', $projectId)
            ->delete();

        $this->newQuery()
            ->whereIn('id', $projectId)
            ->delete();

        return true;
    }

    /**
     * @param array $request
     * @param int $projectId
     * @return Builder[]|Collection
     */
    public function getProjectDataById(array $request, int $projectId): Collection
    {
        return $this->model
            ->newQuery()
            ->with(['folders', 'documents', 'rooms.room.users', 'users.userProjects' =>
                function ($query) use ($projectId) {
                    $query->where('project_id', $projectId);
                }])
            ->where('id', '=', $projectId)
            ->whereNull('deleted_at')
            ->get();
    }

    /**
     * @param array $request
     * @param string $projectId
     *
     * @return object|null
     * @throws FileNotFoundException
     */
    public function addInBin(array $request, string $projectId): ?object
    {
        $projectData = [];
        $now = Carbon::now();
        $projects = $this->newQuery()->find(explode(',', $projectId));

        $this->userProjectRepository
            ->newQuery()
            ->whereIn('project_id', explode(',', $projectId))
            ->update(['deleted_at' => $now]);

        if (is_null($projects)) {
            return null;
        }

        foreach ($projects as $project) {
            $projectData[$project->id] = $project->id;
            $myRoom = $project->rooms()->get();
            $myFolders = $project->folders()->get();
            $myDocuments = $project->documents()->get();
            if (count($myRoom)) {
                $this->roomRepository->addInBin($request, "", $myRoom);
            }
            if (count($myFolders)) {
                $this->folderRepository->addInBin($request, "", $myFolders);
            }

            if (count($myDocuments)) {
                $this->documentRepository->addInBin($request, "", $myDocuments);
            }
            $project->update(['deleted_at' => $now]);
        }

        if (Storage::disk("public")->exists("{$request['auth_user_id']}/project.txt")) {
            $cachedProjects = json_decode(Storage::disk("public")->get("{$request['auth_user_id']}/project.txt"), true);

            foreach ($cachedProjects as $key => $value) {
                if (!isset($projectData[$key])) {
                    $projectData[$key] = $value;
                };
            }
        }

        Storage::disk("public")->put("{$request['auth_user_id']}/project.txt", json_encode($projectData));

        return $projects;
    }

    /**
     * @param array $request
     * @return Collection
     */
    public function getProjects(array $request): Collection
    {
        $query = $request['User']
            ->newQuery()
            ->where('id', '=', $request['auth_user_id'])
            ->with('projects.project', function ($query) use ($request) {

                if (isset($request['query']['offset']) && isset($request['query']['limit'])) {
                    $query = $query
                        ->offset($request['query']['offset'] - 1)
                        ->limit($request['query']['limit']);
                }

                if (isset($request['query']['type']) && $request['query']['type'] === 'deleted') {
                    $query = $query->whereNotNull('deleted_at');
                } else {
                    $query = $query->whereNull('deleted_at');
                }

                return $query;
            })->withCount('projects as project_count');

        $projectIds = [];

        foreach ($query->get()->toArray() as $user) {
            foreach ($user['projects'] as $project) {
                $projectIds[] = $project['id'];
            };
        }

        $deletedProjectIds = $this->model
            ->newQuery()
            ->whereIn('id', $projectIds)
            ->whereNotNull('deleted_at')
            ->get('id');

        $this->userProjectRepository
            ->newQuery()
            ->whereIn('project_id', array_values($deletedProjectIds->toArray()))
            ->update(['deleted_at' => Carbon::now()]);

        return $query->get();
    }


    /**
     * @param array $request
     * @param $projectId
     * @return Builder|Collection|\Illuminate\Database\Eloquent\Model|object
     */
    public function getProjectMembers(array $request, $projectId): ?Model
    {
        return $this->model
            ->newQuery()
            ->with(['users.userProjects' => function ($query) use ($projectId) {
                $query->where('project_id', '=', $projectId);
            }])
            ->where('id', '=', $projectId)
            ->whereNull('deleted_at')
            ->first();
    }
}
