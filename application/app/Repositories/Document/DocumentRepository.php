<?php

namespace App\Repositories\Document;

use App\Entity\DocumentPackages\Document;
use App\Repositories\AbstractRepository;
use App\Repositories\User\UserDocumentRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use stdClass;

class DocumentRepository extends AbstractRepository
{
    private UserDocumentRepository $userDocumentRepository;

    /**
     * DocumentRepository constructor.
     * @param UserDocumentRepository $userDocumentRepository
     */
    public function __construct(UserDocumentRepository $userDocumentRepository)
    {
        $this->model = new Document();
        $this->userDocumentRepository = $userDocumentRepository;
    }

    /**
     * @param string $documentId
     *
     * @return bool
     * @throws FileNotFoundException
     */
    public function deleteManyDocuments(string $documentId, $model = null): bool
    {
        $cachedDocuments = null;
        $docsId = [];
        if (Storage::disk('public')->exists('document.txt')) {
            $cachedDocuments = json_decode(Storage::disk('public')->get('document.txt'), true);
        }

        if ($model) {
            $documents = $model;
        } else {
            $documents = $this
                ->model
                ->whereIn('id', explode(',', $documentId))->get();
        }

        foreach ($documents as $document) {
            $docsId[] = $document->id;
            $key = array_search($document->id, $cachedDocuments ?? []);

            if (false !== $key) {
                unset($cachedDocuments[$key]);
                if (!$cachedDocuments) {
                    Storage::disk('public')->delete('document.txt');
                } else {
                    Storage::disk('public')->put('document.txt', json_encode($cachedDocuments));
                }
            }
        }

        $this->userDocumentRepository
            ->newQuery()
            ->whereIn('document_id', $docsId)
            ->delete();

        $this->newQuery()
            ->whereIn('id', $docsId)
            ->delete();

        return true;
    }

    /**
     * @param array $request
     * @param int $documentId
     * @return Builder[]|Collection
     */
    public function getDocumentDataById(array $request, int $documentId): Collection
    {
        return $this->model->with(['users.documents' => function ($query) use ($documentId) {
            $query->where('document_id', $documentId);
        }])->where('id', '=', $documentId)
            ->get();
    }

    /**
     * @param array $request
     * @param string $documentId
     * @return object|null
     * @throws FileNotFoundException
     */
    public function addInBin(array $request, string $documentId, $model = null): ?object
    {
        $documentData = [];
        $documentIds = [];

        $now = Carbon::now();
        if ($model) {
            $documents = $model;
        } else {
            $documents = $this
                ->newQuery()
                ->whereIn('id', explode(',', $documentId))->get();
        }


        foreach ($documents as $document) {
            $documentIds[] = $document->id;
            $documentData[$document->id] = $document->id;
            $document->update(['deleted_at' => $now]);
        }

        $this->userDocumentRepository
            ->newQuery()
            ->whereIn('document_id', $documentIds)
            ->update(['deleted_at' => $now]);


        if (Storage::disk("public")->exists("{$request['auth_user_id']}/document.txt")) {
            $cachedDocuments = json_decode(Storage::disk("public")
                ->get("{$request['auth_user_id']}/document.txt"), true);
            foreach ($cachedDocuments as $key => $value) {
                if (!isset($documentData[$key])) {
                    $documentData[$key] = $key;
                }
            }
        }

        Storage::disk("public")->put("{$request['auth_user_id']}/document.txt", json_encode($documentData));

        return new stdClass();
    }


    /**
     * @param array $request
     * @return Collection
     */
    public function getDocuments(array $request): Collection
    {
        $query = $request['User']
            ->newQuery()
            ->where('id', '=', $request['auth_user_id'])
            ->with('documents.document', function ($query) use ($request) {

                if (isset($request['query']['offset']) && isset($request['query']['limit'])) {
                    $query = $query
                        ->offset($request['query']['offset'] - 1)
                        ->limit($request['query']['limit']);
                }

                if (isset($request['query']['type']) && $request['query']['type'] === 'deleted') {
                    $query = $query->whereNotNull('deleted_at');
                } else {
                    $query = $query->whereNull('deleted_at');
                }

                return $query;
            })->withCount('documents as document_count');

        $documentIds = [];

        foreach ($query->get()->toArray() as $user) {
            foreach ($user['documents'] as $document) {
                $documentIds[] = $document['id'];
            };
        }

        $deletedDocumentIds = $this->model
            ->newQuery()
            ->whereIn('id', $documentIds)
            ->whereNotNull('deleted_at')
            ->get('id');

        $this->userDocumentRepository
            ->newQuery()
            ->whereIn('document_id', array_values($deletedDocumentIds->toArray()))
            ->update(['deleted_at' => Carbon::now()]);

        return $query->get();
    }
}
