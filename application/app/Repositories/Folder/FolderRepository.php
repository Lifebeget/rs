<?php

namespace App\Repositories\Folder;

use App\Entity\FolderPackages\Folder;
use App\Repositories\AbstractRepository;
use App\Repositories\Document\DocumentRepository;
use App\Repositories\User\UserFolderRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Mixed_;

class FolderRepository extends AbstractRepository
{
    /**
     * @var UserFolderRepository
     * @var DocumentRepository
     */
    private UserFolderRepository $userFolderRepository;
    private DocumentRepository $documentRepository;

    /**
     * FolderRepository constructor.
     * @param UserFolderRepository $userFolderRepository
     * @param DocumentRepository $documentRepository
     */
    public function __construct(
        UserFolderRepository $userFolderRepository,
        DocumentRepository   $documentRepository
    )
    {
        $this->model = new Folder();
        $this->userFolderRepository = $userFolderRepository;
        $this->documentRepository = $documentRepository;
    }

    /**
     * @param string $folderId
     *
     * @return bool
     * @throws FileNotFoundException
     */
    public function deleteManyFolders(array $folderId, $model = null): bool
    {
        $cachedFolders = null;

        if (Storage::disk('public')->exists('folder.txt')) {
            $cachedFolders = json_decode(Storage::disk('public')->get('folder.txt'), true);
        }

        if ($model) {
            $folders = $model;
        } else {
            $folders = $this
                ->model
                ->whereIn('id', $folderId)->get();
        }

        $childFolderDocs = [];
        $foldersChild = [];
        foreach ($folders as $folder) {
            $key = array_search($folder->id, $cachedFolders ?? []);
            $foldersChild = $this->checkChildFolders($folder, $childFolderDocs);
            if (false !== $key) {
                unset($cachedFolders[$key]);
                if (!$cachedFolders) {
                    Storage::disk('public')->delete('folder.txt');
                } else {
                    Storage::disk('public')->put('folder.txt', json_encode($cachedFolders));
                }
            }
        }

        if (isset($foldersChild["document_ids"])) {
            $this->documentRepository->deleteManyDocuments('', $foldersChild["document_ids"]);
        }

        $this->userFolderRepository
            ->newQuery()
            ->whereIn('folder_id', $foldersChild["folder_ids"])
            ->delete();

        $this->newQuery()
            ->whereIn('id', $foldersChild["folder_ids"])
            ->delete();

        return true;
    }

    /**
     * @param array $request
     * @return Collection
     */
    public function getFolders(array $request): Collection
    {
        $query = $request['User']
            ->newQuery()
            ->where('id', '=', $request['auth_user_id'])
            ->with('folders.folder', function ($query) use ($request) {

                if (isset($request['query']['offset']) && isset($request['query']['limit'])) {
                    $query = $query
                        ->offset($request['query']['offset'] - 1)
                        ->limit($request['query']['limit']);
                }

                if (isset($request['query']['type']) && $request['query']['type'] === 'deleted') {
                    $query = $query->whereNotNull('deleted_at');
                } else {
                    $query = $query->whereNull('deleted_at');
                }

                return $query;
            })->withCount('folders as folder_count');

        $folderIds = [];

        foreach ($query->get()->toArray() as $user) {
            foreach ($user['folders'] as $folder) {
                $folderIds[] = $folder['id'];
            };
        }

        $deletedFolderIds = $this->model
            ->newQuery()
            ->whereIn('id', $folderIds)
            ->whereNotNull('deleted_at')
            ->get('id');

        $this->userFolderRepository
            ->newQuery()
            ->whereIn('folder_id', array_values($deletedFolderIds->toArray()))
            ->update(['deleted_at' => Carbon::now()]);

        return $query->get();
    }

    /**
     * @param array $request
     * @param int $folderId
     *
     * @return Builder[]|Collection
     */
    public function getFolderDataById(array $request, int $folderId): Collection
    {
        return Folder::with(['folder.users', 'notDeletedDocuments'])
            ->where('id', '=', $folderId)
            ->get();
    }

    /**
     * @param array $request
     * @param string $folderId
     *
     * @return object|null
     * @throws FileNotFoundException
     */
    public function addInBin(array $request, string $folderId, $model = null): ?object
    {
        $folderData = [];
        $now = Carbon::now();


        if ($model) {
            $folders = $model;
        } else {
            $folders = $this->newQuery()->find(explode(',', $folderId));
        }

        if (is_null($folders)) {
            return null;
        }

        $childFolderDocs = [];
        $foldersChild = [];

        foreach ($folders as $folder) {
            $folderData[$folder->id] = $folder->id;
            $foldersChild = $this->checkChildFolders($folder, $childFolderDocs);
        }
        if (isset($foldersChild['document_ids'])) {
            $this->documentRepository->addInBin($request, "", $foldersChild['document_ids']);
        }

        $this->newQuery()
            ->whereIn('id', $foldersChild['folder_ids'])
            ->update(['deleted_at' => $now]);

        $this->userFolderRepository
            ->newQuery()
            ->whereIn('folder_id', $foldersChild['folder_ids'])
            ->update(['deleted_at' => $now]);

        if (Storage::disk("public")->exists("{$request['auth_user_id']}/folder.txt")) {
            $cachedFolders = json_decode(Storage::disk("public")->get("{$request['auth_user_id']}/folder.txt"), true);
            foreach ($cachedFolders as $key => $value) {
                if (!isset($folderData[$key])) {
                    $folderData[$key] = $key;
                }
            }
        }

        Storage::disk("public")->put("{$request['auth_user_id']}/folder.txt", json_encode($folderData));

        return $folders;
    }

    /**
     * @param Model $folder
     * @param $children
     * @return mixed
     */
    public function checkChildFolders(Model $folder, &$children): Mixed_
    {
        $children["folder_ids"][] = $folder->id;
        $documents = $folder->documents()->get();
        if (count($documents)) {
            foreach ($documents as $doc) {
                $children["document_ids"][] = $doc;
            }
        }

        foreach ($folder->childFolders as $child) {
            $this->checkChildFolders($child, $children);
        }

        return $children;
    }
}
