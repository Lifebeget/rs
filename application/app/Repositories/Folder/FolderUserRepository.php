<?php

namespace App\Repositories\Folder;

use App\Entity\FolderPackages\FolderUser;
use App\Repositories\AbstractRepository;

class FolderUserRepository extends AbstractRepository
{
    /**
     * FolderUserRepository constructor.
     */
    public function __construct()
    {
        $this->model = new FolderUser();
    }
}
