<?php

namespace App\Services\Document;

use App\Entity\DocumentPackages\Document;
use App\Entity\FolderPackages\Folder;
use App\Entity\ProjectPackages\Project;
use App\Entity\RoomPackages\Room;
use App\Exceptions\Common\HttpNotFoundException;
use App\Exceptions\Common\PermissionDeniedException;
use App\Exceptions\Common\UnprocessableEntityException;
use App\Repositories\Document\DocumentRepository;
use App\Repositories\Folder\FolderRepository;
use App\Repositories\Project\ProjectRepository;
use App\Repositories\Room\RoomRepository;
use App\Repositories\User\UserDocumentRepository;
use App\Repositories\User\UserRepository;
use App\Resources\Document\DocumentAndUserFilterResource;
use App\Resources\Document\DocumentResource;
use App\Services\AbstractService;
use App\Services\Invite\InviteService;
use App\Services\User\UserDocumentService;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Spatie\Permission\Models\Permission;
use stdClass;
use Symfony\Component\HttpFoundation\Response;

class DocumentService extends AbstractService
{
    /**
     * @var DocumentRepository
     * @var ProjectRepository
     * @var FolderRepository
     * @var UserRepository
     * @var UserDocumentRepository
     */
    private DocumentRepository $documentRepository;
    private ProjectRepository $projectRepository;
    private FolderRepository $folderRepository;
    private InviteService $inviteService;
    private UserRepository $userRepository;
    private UserDocumentService $userDocumentService;
    private UserDocumentRepository $userDocumentRepository;
    private RoomRepository $roomRepository;

    /**
     * DocumentService constructor.
     *
     * @param DocumentRepository $documentRepository
     * @param ProjectRepository $projectRepository
     * @param FolderRepository $folderRepository
     * @param InviteService $inviteService
     * @param UserRepository $userRepository
     * @param UserDocumentService $userDocumentService
     * @param UserDocumentRepository $userDocumentRepository
     * @param RoomRepository $roomRepository
     */
    public function __construct(
        DocumentRepository     $documentRepository,
        ProjectRepository      $projectRepository,
        FolderRepository       $folderRepository,
        InviteService          $inviteService,
        UserRepository         $userRepository,
        UserDocumentService    $userDocumentService,
        UserDocumentRepository $userDocumentRepository,
        RoomRepository         $roomRepository
    )
    {
        $this->documentRepository = $documentRepository;
        $this->projectRepository = $projectRepository;
        $this->folderRepository = $folderRepository;
        $this->inviteService = $inviteService;
        $this->userRepository = $userRepository;
        $this->userDocumentService = $userDocumentService;
        $this->userDocumentRepository = $userDocumentRepository;
        $this->roomRepository = $roomRepository;
    }

    /**
     * @param array $request
     * @return object
     * @throws HttpNotFoundException
     */
    public function create(array $request): object
    {
        $document = null;
        $documentData = [];
        $documentData['body']['user_id'] = $request['auth_user_id'];

        if (!isset($request['body']['name'])) {
            $request['body']['name'] = 'New document';
        }
        if (isset($request['body']['project_id'])) {
            $project = $this->projectRepository
                ->findOneById($request['body']['project_id']);

            if (is_null($project)) {
                throw new HttpNotFoundException('project_id does not exist');
            }
            $prop["entity_type"] = Project::class;
            $prop["entity_id"] = $request['body']['project_id'];

            $existingName = $this->checkIfDocumentExistsCreate($request['body']['name'], $prop);
            if ($existingName) {
                $newModel = $this->createDocumentVersion($existingName);
                $request['body']['version'] = $newModel->version;
                $request['body']['name'] = $newModel->name;
            }

            $document = $this->documentRepository
                ->create($request['body']);


            if ($document) {
                $documentData['body']['document_id'] = $document->id;
                $documentData['body']['is_owner'] = true;

                if (isset($request['body']['settings'])) {
                    $documentData['body']['settings'] = $request['body']['settings'];
                }

                if (isset($request['body']['is_bookmark'])) {
                    $documentData['body']['is_bookmark'] = $request['body']['is_bookmark'];
                }

                $this->userDocumentService
                    ->create($documentData);
            }

            $project->documents()->save($document);
        }

        if (!isset($request['body']['project_id'])
            && !isset($request['body']['room_id'])
            && !isset($request['body']['folder_id'])
        ) {
            $userDocuments = $request["user"]->directDocuments->whereNull('entity_type');
            if (count($userDocuments)) {
                $userUniqueDocuments = $this->uniqDocumentName(
                    $userDocuments->pluck('name')->toArray(),
                    $request['body']["name"],
                    $userDocuments->max('version')
                );
                $request['body']['name'] = $userUniqueDocuments['name'];
                $request['body']['version'] = $userUniqueDocuments['version'];
            }
            $document = $this->documentRepository
                ->create($request['body']);

            if ($document) {
                $documentData['body']['document_id'] = $document->id;
                $documentData['body']['is_owner'] = true;

                if (isset($request['body']['settings'])) {
                    $documentData['body']['settings'] = $request['body']['settings'];
                }

                if (isset($request['body']['is_bookmark'])) {
                    $documentData['body']['is_bookmark'] = $request['body']['is_bookmark'];
                }

                $this->userDocumentService
                    ->create($documentData);
            }
        }

        if (isset($request['body']['room_id'])) {
            $prop["entity_type"] = Room::class;
            $prop["entity_id"] = $request['body']['room_id'];
            $room = $this->roomRepository
                ->findOneById($request['body']['room_id']);

            if (is_null($room)) {
                throw new HttpNotFoundException('room_id does not exist');
            }
            $existingName = $this->checkIfDocumentExistsCreate($request['body']['name'], $prop);
            if ($existingName) {
                $newModel = $this->createDocumentVersion($existingName);
                $request['body']['version'] = $newModel->version;
                $request['body']['name'] = $newModel->name;
            }

            $document = $this->documentRepository
                ->create($request['body']);

            if ($document) {
                $documentData['body']['document_id'] = $document->id;
                $documentData['body']['is_owner'] = true;

                if (isset($request['body']['settings'])) {
                    $documentData['body']['settings'] = $request['body']['settings'];
                }

                if (isset($request['body']['is_bookmark'])) {
                    $documentData['body']['is_bookmark'] = $request['body']['is_bookmark'];
                }

                $this->userDocumentService
                    ->create($documentData);
            }

            $room->documents()->save($document);
        }

        if (isset($request['body']['folder_id'])) {
            $prop["entity_type"] = Folder::class;
            $prop["entity_id"] = $request['body']['folder_id'];
            $existingName = $this->checkIfDocumentExistsCreate($request['body']['name'], $prop);
            if ($existingName) {
                $newModel = $this->createDocumentVersion($existingName);
                $request['body']['version'] = $newModel->version;
                $request['body']['name'] = $newModel->name;
            }
            $folder = $this->folderRepository
                ->findOneById($request['body']['folder_id']);

            if (is_null($folder)) {
                throw new HttpNotFoundException('folder_id does not exist');
            }

            $document = $this->documentRepository
                ->create($request['body']);

            if ($document) {
                $documentData['body']['document_id'] = $document->id;
                $documentData['body']['is_owner'] = true;

                if (isset($request['body']['settings'])) {
                    $documentData['body']['settings'] = $request['body']['settings'];
                }

                if (isset($request['body']['is_bookmark'])) {
                    $documentData['body']['is_bookmark'] = $request['body']['is_bookmark'];
                }

                $this->userDocumentService
                    ->create($documentData);
            }

            $folder->documents()->save($document);
        }

        Permission::create(['name' => 'documents.*.' . $document->id]);
        $request['user']->givePermissionTo('documents.*.' . $document->id);
        return $document;
    }

    /**
     * @param array $request
     * @param int $documentId
     *
     * @return object
     * @throws HttpNotFoundException
     * @throws PermissionDeniedException
     * @throws UnprocessableEntityException
     */
    public function update(array $request, int $documentId): object
    {
        if ($request['user']->can('documents.*.' . $documentId)) {
            $documentData['body'] = [];
            $doc = $this->documentRepository->findOneById($documentId);
            $doc->name = $request['body']['name'];
            $existingName = $this->checkIfDocumentExists($doc);
            if ($existingName) {
                throw new UnprocessableEntityException('Document name has already been taken', 422);
            }

            if (isset($request['body']['project_id'])) {
                $project = $this->projectRepository
                    ->findOneById($request['body']['project_id']);

                if (is_null($project)) {
                    throw new HttpNotFoundException('project_id does not exist');
                }

                $request['body']['entity_id'] = $request['body']['project_id'];
                $request['body']['entity_type'] = $project->getMorphClass();
            }

            if (isset($request['body']['room_id'])) {
                $room = $this->roomRepository
                    ->findOneById($request['body']['room_id']);

                if (is_null($room)) {
                    throw new HttpNotFoundException('room_id does not exist');
                }

                $request['body']['entity_id'] = $request['body']['room_id'];
                $request['body']['entity_type'] = $room->getMorphClass();
            }

            if (isset($request['body']['folder_id'])) {
                $folder = $this->folderRepository
                    ->findOneById($request['body']['folder_id']);

                if (is_null($folder)) {
                    throw new HttpNotFoundException('folder_id does not exist');
                }

                $request['body']['entity_id'] = $request['body']['folder_id'];
                $request['body']['entity_type'] = $folder->getMorphClass();

            }

            if (isset($request['body']['library']) && $request['body']['library'] === "true") {
                $request['body']['entity_id'] = null;
                $request['body']['entity_type'] = null;
            }

            $documentUpdated = $this->documentRepository
                ->update($request['body'], $documentId);

            if ($documentUpdated) {
                if (isset($request['body']['document_id'])) {
                    $documentData['body']['document_id'] = $documentId;
                }

                if (isset($request['body']['is_owner'])) {
                    $documentData['body']['is_owner'] = $request['body']['is_owner'];
                }

                if (isset($request['body']['is_bookmark'])) {
                    $documentData['body']['is_bookmark'] = $request['body']['is_bookmark'];
                }

                if (isset($request['body']['settings'])) {
                    $documentData['body']['settings'] = json_encode($request['body']['settings']);
                }

                $this->userDocumentRepository
                    ->newQuery()
                    ->where('document_id', '=', $documentId)
                    ->update($documentData['body']);
            }

            return new stdClass();
        }

        throw new PermissionDeniedException('permission denied', Response::HTTP_NOT_MODIFIED);
    }

    /**
     * @param array $request
     * @param string $documentId
     *
     * @return object
     * @throws PermissionDeniedException
     */
    public function updateUserPermissions(array $request, string $documentId): object
    {
        if ($request['user']->can('documents.*.' . $documentId)) {
            $this->userDocumentRepository
                ->newQuery()
                ->where('user_id', '=', $request['body']['user_id'])
                ->where('document_id', '=', (int)$documentId)
                ->update(['settings' => json_encode($request['body']['settings'])]);

            return new stdClass();
        }
        throw new PermissionDeniedException('permission denied', Response::HTTP_NOT_MODIFIED);
    }

    /**
     * @param array $request
     * @param string $documentId
     *
     * @return object
     * @throws PermissionDeniedException
     */
    public function deleteDocumentUser(array $request, string $documentId): object
    {
        if ($request['user']->can('documents.*.' . $documentId)) {
            $this->userDocumentRepository
                ->newQuery()
                ->where('user_id', '=', $request['body']['user_id'])
                ->where('document_id', '=', (int)$documentId)->delete();

            return new stdClass();
        }
        throw new PermissionDeniedException('permission denied', Response::HTTP_FORBIDDEN);
    }

    /**
     * @param array $request
     * @param string $documentId
     * @return object|null
     * @throws FileNotFoundException
     * @throws PermissionDeniedException
     * @throws HttpNotFoundException
     */
    public function moveToBin(array $request, string $documentId): ?object
    {
        $accessDocumentIds = [];

        foreach (explode(',', $documentId) as $id) {
            if ($request['user']->can('documents.*.' . $id)) {
                $accessDocumentIds[] = $id;
            } else {
                throw new PermissionDeniedException('permission denied');
            }
        }

        $document = $this->documentRepository
            ->addInBin($request, implode(',', $accessDocumentIds));

        if (is_null($document)) {
            throw new HttpNotFoundException('document id does not exist');
        }

        return new stdClass();
    }

    /**
     * @param array $request
     * @param string $documentId
     *
     * @return bool
     * @throws FileNotFoundException
     * @throws PermissionDeniedException
     */
    public function delete(array $request, string $documentId): bool
    {
        $accessDocumentIds = [];
        foreach (explode(',', $documentId) as $id) {
            if ($request['user']->can('documents.*.' . $id)) {
                $accessDocumentIds[] = $id;
            } else {
                throw new PermissionDeniedException('permission denied', Response::HTTP_FORBIDDEN);
            }
        }

        return $this->documentRepository
            ->deleteManyDocuments(implode(',', $accessDocumentIds));
    }

    /**
     * @param array $request
     * @param int $documentId
     * @return AnonymousResourceCollection
     * @throws PermissionDeniedException
     */
    public function getDocumentById(array $request, int $documentId): AnonymousResourceCollection
    {
        $userDocument = $this->userDocumentRepository->newQuery()
            ->where('user_id', '=', $request['auth_user_id'])
            ->where('document_id', '=', (int)$documentId)->first();
        if (is_null($userDocument)) {
            throw new PermissionDeniedException('permission denied', Response::HTTP_FORBIDDEN);
        }
        return DocumentAndUserFilterResource::collection(
            $this->documentRepository
                ->getDocumentDataById($request, $documentId)
        );
    }

    /**
     * @param array $queryOptions
     * @return AnonymousResourceCollection
     */
    public function getDocuments(array $queryOptions): AnonymousResourceCollection
    {
        $queryOptions['User'] = $this->userRepository
            ->getModel();

        return DocumentResource::collection(
            $this->documentRepository
                ->getDocuments($queryOptions)
        );
    }

    /**
     * @param array $request
     *
     * @return object
     * @throws PermissionDeniedException
     */
    public function sendInvite(array $request): object
    {
        return $this->inviteService
            ->sendInvite($request, 'invite', Document::class);
    }

    /**
     * @param array $queryOptions
     *
     * @return array|null
     * @throws \App\Exceptions\Auth\AuthenticateException
     */
    public function acceptInvite(array $queryOptions): ?array
    {
        return $this->inviteService
            ->acceptInvite($queryOptions, $this->userDocumentService);
    }

    /**
     * @param array $request
     *
     * @return object
     */
    public function generateCopyLink(array $request): object
    {
        return $this->inviteService->generateInviteLink($request);
    }

    /**
     * @param array $request
     *
     * @return array
     */
    public function acceptLinkInvite(array $request): array
    {
        return $this->inviteService->acceptInviteLink($request, $this->userDocumentService);
    }

    /**
     * @param Model $requestData
     * @return int
     */
    public function checkIfDocumentExists(Model $requestData): int
    {
        return $this->documentRepository->newQuery()
            ->where('name', $requestData['name'])
            ->where('entity_type', $requestData["entity_type"])
            ->where('entity_id', $requestData['entity_id'])
            ->where('id', '!=', $requestData["id"])
            ->count();
    }

    /**
     * @param array $requestData
     * @param $entity_type
     * @param $entity_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function checkIfDocumentExistsCreate($name, $prop)
    {
        return $this->documentRepository->newQuery()
            ->where('name', $name)
            ->where('entity_type', $prop["entity_type"])
            ->where('entity_id', $prop["entity_id"])
            ->first();
    }

    /**
     * @param object $model
     * @return object
     */
    public function createDocumentVersion(object $model): object
    {
        $name = explode(' ', $model->name);
        $maxVersion = $this->documentRepository->newQuery()
            ->where('entity_type', $model["entity_type"])
            ->where('entity_id', $model["entity_id"])
            ->max('version');
        $model->version = ++$maxVersion;

        if (intval($name[count($name) - 1])) {
            $name[count($name) - 1] = $model->version;
            $model->name = implode(" ", $name);
        } else {
            $model->name .= " " . $model->version;
        }
        $doc = $this->checkIfDocumentExistsCreate($model->name, $model);
        if ($doc) {
            $doc->version = $model->version;
            $doc->save();
            return $this->createDocumentVersion($doc);
        } else {
            return $model;
        }
    }

    /**
     * @param array $names
     * @param $reqName
     * @param $version
     * @return array
     */
    public function uniqDocumentName(array $names, $reqName, $version): array
    {
        $name = explode(' ', $reqName);
        $data = array_search($reqName, $names);
        $newVersion = $version;
        if ($data !== false) {
            $newVersion = ++$newVersion;
            $nameLastIndex = count($name) - 1;
            if (intval($name[$nameLastIndex])) {
                $name[$nameLastIndex] = $newVersion;
                $name = implode(" ", $name);
            } else {
                $name = $reqName . " " . $newVersion;
            }
            return $this->uniqDocumentName($names, $name, $newVersion);
        }
        return [
            'name' => implode(" ", $name),
            'version' => $version
        ];
    }
}
