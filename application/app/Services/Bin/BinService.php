<?php

namespace App\Services\Bin;

use App\Repositories\Document\DocumentRepository;
use App\Repositories\Folder\FolderRepository;
use App\Repositories\Project\ProjectRepository;
use App\Repositories\Room\RoomRepository;
use App\Repositories\User\UserDocumentRepository;
use App\Repositories\User\UserFolderRepository;
use App\Repositories\User\UserProjectRepository;
use App\Repositories\User\UserRoomRepository;
use App\Services\AbstractService;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;

class BinService extends AbstractService
{
    /**
     * @var $roomRepository
     * @var $projectRepository
     * @var $folderRepository
     * @var $documentRepository
     */
    private RoomRepository $roomRepository;
    private ProjectRepository $projectRepository;
    private FolderRepository $folderRepository;
    private DocumentRepository $documentRepository;
    private UserProjectRepository $userProjectRepository;
    private UserRoomRepository $userRoomRepository;
    private UserFolderRepository $userFolderRepository;
    private UserDocumentRepository $userDocumentRepository;

    /**
     * BinService constructor.
     *
     * @param RoomRepository $roomRepository
     * @param ProjectRepository $projectRepository
     * @param FolderRepository $folderRepository
     * @param DocumentRepository $documentRepository
     * @param UserProjectRepository $userProjectRepository
     * @param UserRoomRepository $userRoomRepository
     * @param UserFolderRepository $userFolderRepository
     * @param UserDocumentRepository $userDocumentRepository
     */
    public function __construct(
        RoomRepository         $roomRepository,
        ProjectRepository      $projectRepository,
        FolderRepository       $folderRepository,
        DocumentRepository     $documentRepository,
        UserProjectRepository  $userProjectRepository,
        UserRoomRepository     $userRoomRepository,
        UserFolderRepository   $userFolderRepository,
        UserDocumentRepository $userDocumentRepository
    )
    {
        $this->roomRepository = $roomRepository;
        $this->projectRepository = $projectRepository;
        $this->folderRepository = $folderRepository;
        $this->documentRepository = $documentRepository;
        $this->userProjectRepository = $userProjectRepository;
        $this->userRoomRepository = $userRoomRepository;
        $this->userFolderRepository = $userFolderRepository;
        $this->userDocumentRepository = $userDocumentRepository;
    }

    /**
     * @param array $request
     * @return array|null
     * @throws FileNotFoundException
     */
    public function getBinData(array $request): ?array
    {
        $data['folders'] = [];
        $data['documents'] = [];
        $data['rooms'] = [];
        $data['projects'] = [];

        if (Storage::disk("public")->exists("{$request['auth_user_id']}/folder.txt")) {
            $deletedFolders = json_decode(Storage::disk("public")->get("{$request['auth_user_id']}/folder.txt"), true);
            $data['folders'] = $this
                ->folderRepository
                ->newQuery()
                ->whereIn('id', array_values($deletedFolders))
                ->get();
        }

        if (Storage::disk("public")->exists("{$request['auth_user_id']}/document.txt")) {
            $deletedDocuments = json_decode(Storage::disk("public")
                ->get("{$request['auth_user_id']}/document.txt"), true);
            $data['documents'] = $this
                ->documentRepository
                ->newQuery()
                ->whereIn('id', array_values($deletedDocuments))
                ->get();
        }

        if (Storage::disk("public")->exists("{$request['auth_user_id']}/project.txt")) {
            $deletedProjects = json_decode(Storage::disk("public")
                ->get("{$request['auth_user_id']}/project.txt"), true);
            $data['projects'] = $this
                ->projectRepository
                ->newQuery()
                ->whereIn('id', array_values($deletedProjects))
                ->get();
        }

        if (Storage::disk("public")->exists("{$request['auth_user_id']}/room.txt")) {
            $deletedRooms = json_decode(Storage::disk("public")
                ->get("{$request['auth_user_id']}/room.txt"), true);
            $data['rooms'] = $this
                ->roomRepository
                ->newQuery()
                ->whereIn('id', array_values($deletedRooms))
                ->get();
        }

        return $data;
    }

    /**
     * @param array $request
     * @return bool
     */
    public function deleteBinData(array $request): bool
    {
        $userFolders = $this->userFolderRepository
            ->newQuery()
            ->where('user_id', '=', $request['auth_user_id'])
            ->whereNotNull('deleted_at');

        $folderIds = [];

        foreach ($userFolders->get() as $userFolder) {
            $folderIds[] = $userFolder->folder_id;
        }

        $userFolders->delete();

        $this->folderRepository
            ->newQuery()
            ->whereIn('id', $folderIds)
            ->delete();

        $userDocuments = $this->userDocumentRepository
            ->newQuery()
            ->where('user_id', '=', $request['auth_user_id'])
            ->whereNotNull('deleted_at');

        $documentIds = [];

        foreach ($userDocuments->get() as $userDocument) {
            $documentIds[] = $userDocument->document_id;
        }

        $userDocuments->delete();

        $this->documentRepository
            ->newQuery()
            ->whereIn('id', $documentIds)
            ->delete();

        $userProjects = $this->userProjectRepository
            ->newQuery()
            ->where('user_id', '=', $request['auth_user_id'])
            ->whereNotNull('deleted_at');
        $projectIds = [];

        foreach ($userProjects->get() as $userProject) {
            $projectIds[] = $userProject->project_id;
        }

        $userProjects->delete();

        $this->projectRepository
            ->newQuery()
            ->whereIn('id', $projectIds)
            ->delete();

        $userRooms = $this->userRoomRepository
            ->newQuery()
            ->where('user_id', '=', $request['auth_user_id'])
            ->whereNotNull('deleted_at');

        $roomIds = [];

        foreach ($userRooms->get() as $userRoom) {
            $roomIds[] = $userRoom->room_id;
        }

        $userRooms->delete();

        $this->roomRepository
            ->newQuery()
            ->whereIn('id', $roomIds)
            ->delete();

        Storage::disk("public")
            ->delete([
                "{$request['auth_user_id']}/project.txt",
                "{$request['auth_user_id']}/folder.txt",
                "{$request['auth_user_id']}/document.txt",
                "{$request['auth_user_id']}/room.txt"
            ]);

        return true;
    }

    /**
     * @param array $request
     * @return bool
     * @throws FileNotFoundException
     */
    public function restoreBinData(array $request): bool
    {
        if (Storage::disk("public")->exists("{$request['auth_user_id']}/folder.txt")) {
            $deletedFolders = json_decode(Storage::disk("public")
                ->get("{$request['auth_user_id']}/folder.txt"), true);
            $this->folderRepository
                ->newQuery()
                ->whereIn('id', array_values($deletedFolders))
                ->update(['deleted_at' => null]);

            $this->userFolderRepository
                ->newQuery()
                ->whereIn('folder_id', array_values($deletedFolders))
                ->update(['deleted_at' => null]);
        }

        if (Storage::disk("public")->exists("{$request['auth_user_id']}/document.txt")) {
            $deletedDocuments = json_decode(Storage::disk("public")
                ->get("{$request['auth_user_id']}/document.txt"), true);
            $this->documentRepository
                ->newQuery()
                ->whereIn('id', array_values($deletedDocuments))
                ->update(['deleted_at' => null]);

            $this->userDocumentRepository
                ->newQuery()
                ->whereIn('document_id', array_values($deletedDocuments))
                ->update(['deleted_at' => null]);
        }

        if (Storage::disk("public")->exists("{$request['auth_user_id']}/project.txt")) {
            $deletedProjects = json_decode(Storage::disk("public")
                ->get("{$request['auth_user_id']}/project.txt"), true);
            $this->projectRepository
                ->newQuery()
                ->whereIn('id', array_values($deletedProjects))
                ->update(['deleted_at' => null]);

            $this->userProjectRepository
                ->newQuery()
                ->whereIn('project_id', array_values($deletedProjects))
                ->update(['deleted_at' => null]);
        }

        if (Storage::disk("public")->exists("{$request['auth_user_id']}/room.txt")) {
            $deletedRooms = json_decode(Storage::disk("public")
                ->get("{$request['auth_user_id']}/room.txt"), true);
            $this->roomRepository
                ->newQuery()
                ->whereIn('id', array_values($deletedRooms))
                ->update(['deleted_at' => null]);

            $this->userRoomRepository
                ->newQuery()
                ->whereIn('room_id', array_values($deletedRooms))
                ->update(['deleted_at' => null]);
        }

        Storage::disk("public")
            ->delete([
                "{$request['auth_user_id']}/project.txt",
                "{$request['auth_user_id']}/folder.txt",
                "{$request['auth_user_id']}/document.txt",
                "{$request['auth_user_id']}/room.txt"
            ]);

        return true;
    }

    /**
     * @param array $request
     * @return bool
     * @throws FileNotFoundException
     */
    public function restoreSingleEntitiesInBin(array $request): bool
    {
        if (isset($request['body']['project_id'])) {
            return $this->restoreProjects($request);
        }

        if (isset($request['body']['room_id'])) {
            return $this->restoreRooms($request);
        }

        if (isset($request['body']['folder_id'])) {
            return $this->restoreFolders($request);
        }

        if (isset($request['body']['document_id'])) {
            return $this->restoreDocuments($request);
        }
        return true;
    }

    /**
     * @param array $request
     * @return bool
     * @throws FileNotFoundException
     */
    public function restoreProjects(array $request): bool
    {
        $this->restoreProjectRoomsFoldersDocuments($request);
        return true;
    }

    /**
     * @param array $request
     * @throws FileNotFoundException
     */
    public function restoreProjectRoomsFoldersDocuments(array $request): void
    {
        $projectIds = [];
        $cachedProjects = null;
        if (Storage::disk("public")->exists("{$request['auth_user_id']}/project.txt")) {
            $cachedProjects = json_decode(Storage::disk("public")
                ->get("{$request['auth_user_id']}/project.txt"), true);
        }


        $projects = $this->projectRepository
            ->newQuery()
            ->whereIn('id', $request['body']['project_id'])
            ->get();

        foreach ($projects as $project) {
            $projectIds[] = $project->id;

            $room = $project->rooms()->get();
            if (count($room)) {
                $this->restoreRoomsFoldersDocuments($request, $room);
            }

            $folders = $project->folders()->get();
            if (count($folders)) {
                $this->restoreFolders($request, $folders);
            }

            $documents = $project->documents()->get();
            if (count($documents)) {
                $this->restoreDocuments($request, $documents);
            }

            $key = array_search($project->id, $cachedProjects ?? []);

            if (false !== $key) {
                unset($cachedProjects[$key]);
                if (!$cachedProjects) {
                    Storage::disk("public")
                        ->delete("{$request['auth_user_id']}/project.txt");
                } else {
                    Storage::disk("public")
                        ->put("{$request['auth_user_id']}/project.txt", json_encode($cachedProjects));
                }
            }
        }

        $this->userProjectRepository
            ->newQuery()
            ->whereIn('project_id', $projectIds)
            ->update(['deleted_at' => null]);

        $this->projectRepository
            ->newQuery()
            ->whereIn('id', $projectIds)
            ->update(['deleted_at' => null]);
    }

    /**
     * @param array $request
     * @return bool
     * @throws FileNotFoundException
     */
    public function restoreRooms(array $request): bool
    {
        $this->restoreRoomsFoldersDocuments($request);
        return true;
    }

    /**
     * @param array $request
     * @throws FileNotFoundException
     */
    public function restoreRoomsFoldersDocuments(array $request, $model = null): void
    {
        $roomsId = [];
        $cachedRooms = null;
        if (Storage::disk("public")->exists("{$request['auth_user_id']}/room.txt")) {
            $cachedRooms = json_decode(Storage::disk("public")
                ->get("{$request['auth_user_id']}/room.txt"), true);
        }

        if ($model) {
            $rooms = $model;
        } else {
            $rooms = $this->roomRepository
                ->newQuery()
                ->whereIn('id', $request['body']['room_id'])
                ->get();
        }

        foreach ($rooms as $room) {
            $roomsId[] = $room->id;
            $folders = $room->folders()->get();
            if (count($folders)) {
                $this->restoreFolders($request, $folders);
            }
            $documents = $room->documents()->get();
            if (count($documents)) {
                $this->restoreDocuments($request, $documents);
            }


            $key = array_search($room->id, $cachedRooms ?? []);

            if (false !== $key) {
                unset($cachedRooms[$key]);
                if (!$cachedRooms) {
                    Storage::disk("public")->delete("{$request['auth_user_id']}/room.txt");
                } else {
                    Storage::disk("public")->put("{$request['auth_user_id']}/room.txt", json_encode($cachedRooms));
                }
            }
        }
        $this->roomRepository
            ->newQuery()
            ->whereIn('id', $roomsId)
            ->update(['deleted_at' => null]);

        $this->userRoomRepository
            ->newQuery()
            ->whereIn('room_id', $roomsId)
            ->update(['deleted_at' => null]);
    }

    /**
     * @param array $request
     * @return bool
     * @throws FileNotFoundException
     */
    public function restoreFolders(array $request, $model = null): bool
    {
        $cachedFolders = null;
        if (Storage::disk("public")->exists("{$request['auth_user_id']}/folder.txt")) {
            $cachedFolders = json_decode(Storage::disk("public")
                ->get("{$request['auth_user_id']}/folder.txt"), true);
        }

        if ($model) {
            $folders = $model;
        } else {
            $folders = $this->folderRepository
                ->newQuery()
                ->whereIn('id', $request['body']['folder_id'])
                ->get();
        }

        $childFolderDocs = [];
        $foldersChild = [];

        foreach ($folders as $folder) {
            $foldersChild = $this->folderRepository->checkChildFolders($folder, $childFolderDocs);

            $key = array_search($folder->id, $cachedFolders ?? []);
            if (false !== $key) {
                unset($cachedFolders[$key]);
                if (!$cachedFolders) {
                    Storage::disk("public")->delete("{$request['auth_user_id']}/folder.txt");
                } else {
                    Storage::disk("public")->put("{$request['auth_user_id']}/folder.txt", json_encode($cachedFolders));
                }
            }
        }

        if (isset($foldersChild['document_ids'])) {
            $this->restoreDocuments($request, $foldersChild['document_ids']);
        }
        $this->folderRepository
            ->newQuery()
            ->whereIn('id', $foldersChild['folder_ids'])
            ->update(['deleted_at' => null]);

        $this->userFolderRepository
            ->newQuery()
            ->whereIn('folder_id', $foldersChild['folder_ids'])
            ->where('user_id', '=', $request['auth_user_id'])
            ->update(['deleted_at' => null]);

        return true;
    }

    /**
     * @param array $request
     * @return bool
     * @throws FileNotFoundException
     */
    public function restoreDocuments(array $request, $model = null): bool
    {
        $documentsId = [];
        $cachedDocuments = null;
        if (Storage::disk("public")->exists("{$request['auth_user_id']}/document.txt")) {
            $cachedDocuments = json_decode(Storage::disk("public")
                ->get("{$request['auth_user_id']}/document.txt"), true);
        }


        if ($model) {
            $documents = $model;
        } else {
            $documents = $this->documentRepository
                ->newQuery()
                ->whereIn('id', $request['body']['document_id'])
                ->get();
        }


        foreach ($documents as $document) {
            $documentsId[] = $document->id;
            $key = array_search($document->id, $cachedDocuments ?? []);

            if (false !== $key) {
                unset($cachedDocuments[$key]);
                if (!$cachedDocuments) {
                    Storage::disk("public")
                        ->delete("{$request['auth_user_id']}/document.txt");
                } else {
                    Storage::disk("public")
                        ->put("{$request['auth_user_id']}/document.txt", json_encode($cachedDocuments));
                }
            }
        }

        $this->userDocumentRepository
            ->newQuery()
            ->whereIn('document_id', $documentsId)
            ->update(['deleted_at' => null]);

        $this->documentRepository
            ->newQuery()
            ->whereIn('id', $documentsId)
            ->update(['deleted_at' => null]);
        return true;
    }
}
