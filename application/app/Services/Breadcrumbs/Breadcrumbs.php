<?php

namespace App\Services\Breadcrumbs;

class Breadcrumbs
{
    /**
     * @var array
     */
    private static array $breadcrumbs = [];

    /**
     * @param object $model
     * @return array
     */
    public static function breadcrumbs(object $model): array
    {
        $project = null;
        if ($model->getTable() === 'rooms') {
            $project = $model->project;
        }
        if ($project) {
            self::$breadcrumbs[] = [
                "name" => $project->name,
                "id" => $project->id,
                "type" => "project",
                "entity_id" => $project->id,
                "entity_type" => "project",
            ];
            return self::streamlineBreadcrumbs(self::$breadcrumbs);
        }

        $entity = $model->entity;
        if ($entity) {
            $type = $entity->getTable();
            if ($type === "folders") {
                $type = "folder";
            } elseif ($type === "rooms") {
                $type = "room";
            }
            self::$breadcrumbs[] = [
                "name" => $entity->name,
                "id" => $entity->id,
                "type" => $type,
                "entity_id" => $entity->id,
                "entity_type" => $entity->getTable(),
            ];


            return self::breadcrumbs($entity);
        }
        return self::streamlineBreadcrumbs(self::$breadcrumbs);
    }

    /**
     * @param array $breadcrumb
     * @return array
     */
    private static function streamlineBreadcrumbs(array $breadcrumb): array
    {
        $length = count($breadcrumb);
        $f = 1;
        for ($i = 0; $i < $length; $i++) {
            if ($f !== $length) {
                $breadcrumb[$i]['entity_type'] = $breadcrumb[$f]['entity_type'];
                $breadcrumb[$i]['entity_id'] = $breadcrumb[$f]['entity_id'];
            } else {
                $breadcrumb[$i]['entity_type'] = null;
                $breadcrumb[$i]['entity_id'] = null;
            }
            $f++;
        }
        return $breadcrumb;
    }
}


