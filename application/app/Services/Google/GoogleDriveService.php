<?php

namespace App\Services\Google;

use App\Entity\UserPackages\User;
use App\Exceptions\Common\HttpNotFoundException;
use App\Jobs\GoogleDriveFileExport;
use App\Services\Document\DocumentService;
use Google_Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\RedirectResponse;

class GoogleDriveService
{

    /**
     * @var DocumentService
     */
    private DocumentService $documentService;

    public function __construct(DocumentService $documentService)
    {
        $this->documentService = $documentService;
    }

    /**
     * @return Google_Client
     */
    public function client(): Google_Client
    {
        $client = new Google_Client();
        $client->setClientId(env('GOOGLE_DRIVE_CLIENT_ID'));
        $client->setClientSecret(env('GOOGLE_DRIVE_CLIENT_SECRET'));
        $client->setRedirectUri(env('GOOGLE_DRIVE_REDIRECT_URL'));
        $client->setScopes(explode(',', env('GOOGLE_DRIVE_SCOPES')));
        $client->setApprovalPrompt(env('GOOGLE_DRIVE_APPROVAL_PROMPT'));
        $client->setAccessType(env('GOOGLE_DRIVE_ACCESS_TYPE'));
        return $client;
    }

    /**
     * @param int $user_id
     * @return mixed|RedirectResponse
     */
    public function getDocumentsInDrive(int $user_id)
    {
        try {
            $user = User::find($user_id);
            if (!$user->google_access_token) {
                return $this->toDriveGoogleAuth();
            }
            $access_token = $user->google_access_token;
            $host_google_apis = env('GOOGLE_HOST');
            $url = '/drive/v3/files';
            $q = '?q=mimeType=\'application/vnd.google-apps.document\'';
            $httpRequest = Http::withToken($access_token)->get($host_google_apis . $url . $q);
            return json_decode($httpRequest->body())->files;
        } catch (\Exception $e) {
            return $this->toDriveGoogleAuth();
        }
    }

    /**
     * @param array $files
     * @return bool
     * @throws HttpNotFoundException
     */
    public function exportGoogleDocContent(array $files): bool
    {
        $documents = [];
        $userDocument['auth_user_id'] = Auth::id();
        $userDocument['user'] = Auth::user();
        foreach ($files as $file) {
            $document = [];
            $userDocument['body']['name'] = $file["name"];
            $document["id"] = $this->documentService->create($userDocument)->id;
            $document["content"] = $this->export($file["id"]);
            $documents[] = $document;
        }
        GoogleDriveFileExport::dispatch($documents)->allOnConnection("rabbitmq");
        return true;
    }

    /**
     * @param $file
     * @return string
     */

    public function export(string $fileID): string
    {
        $access_token = Auth::user()->google_access_token;
        $host_google_apis = env('GOOGLE_HOST');
        $url = '/drive/v3/files/';
        $file_id = $fileID; // "1ETyJWSCt-ZBfTN8TBeRgr6lJBDKUms_DFrcAsUArmB8";
        $export = '/export?mimeType=text%2Fhtml&fields=file&prettyPrint=true&key=';
        try {
            $httpRequest = Http::withToken($access_token)->get($host_google_apis . $url . $file_id . $export);
            if ($httpRequest->status() == 200) {
                return $httpRequest->body();
            } else {
                return 'Unexpected HTTP status: ' . $httpRequest->status();
            }
        } catch (\Exception $e) {
            return 'Error: ' . $e->getMessage();
        }
    }


    /**
     * @return RedirectResponse
     */
    public function toDriveGoogleAuth(): RedirectResponse
    {
        $parameters = [
            'access_type' => 'offline',
            'approval_prompt' => 'force',
            'state' => 1
        ];

        return Socialite::driver('google')->redirectUrl(env('GOOGLE_DRIVE_REDIRECT_URL'))
            ->scopes([env('GOOGLE_DRIVE_SCOPES')])
            ->with($parameters)->redirect();
    }
}
