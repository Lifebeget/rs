<?php

namespace App\Services\Folder;

use App\Entity\FolderPackages\Folder;
use App\Entity\RoomPackages\Room;
use App\Exceptions\Common\HttpNotFoundException;
use App\Exceptions\Common\PermissionDeniedException;
use App\Exceptions\Common\UnprocessableEntityException;
use App\Repositories\Folder\FolderRepository;
use App\Repositories\Project\ProjectRepository;
use App\Repositories\Room\RoomRepository;
use App\Repositories\User\UserFolderRepository;
use App\Repositories\User\UserRepository;
use App\Resources\Folder\FolderAndUserFilterResource;
use App\Resources\Folder\FolderResource;
use App\Services\AbstractService;
use App\Services\Auth\PassportService;
use App\Services\Invite\InviteService;
use App\Services\User\UserFolderService;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Spatie\Permission\Models\Permission;
use stdClass;

class FolderService extends AbstractService
{
    /**
     * @var FolderRepository
     * @var UserFolderRepository
     * @var UserFolderService
     * @var InviteService
     * @var PassportService
     * @var ProjectRepository
     * @var RoomRepository
     */
    private FolderRepository $folderRepository;
    private UserRepository $userRepository;
    private UserFolderRepository $userFolderRepository;
    private ProjectRepository $projectRepository;
    private RoomRepository $roomRepository;
    private UserFolderService $userFolderService;
    private InviteService $inviteService;

    /**
     * FileService constructor.
     *
     * @param FolderRepository $folderRepository
     * @param UserRepository $userRepository
     * @param UserFolderRepository $userFolderRepository
     * @param ProjectRepository $projectRepository
     * @param RoomRepository $roomRepository
     * @param InviteService $inviteService
     * @param UserFolderService $userFolderService
     */
    public function __construct(
        FolderRepository     $folderRepository,
        UserRepository       $userRepository,
        UserFolderRepository $userFolderRepository,
        ProjectRepository    $projectRepository,
        RoomRepository       $roomRepository,
        InviteService        $inviteService,
        UserFolderService    $userFolderService
    )
    {
        $this->folderRepository = $folderRepository;
        $this->userRepository = $userRepository;
        $this->userFolderRepository = $userFolderRepository;
        $this->projectRepository = $projectRepository;
        $this->roomRepository = $roomRepository;
        $this->inviteService = $inviteService;
        $this->folderRepository = $folderRepository;
        $this->userFolderService = $userFolderService;
    }

    /**
     * @param array $request
     *
     * @return object
     * @throws HttpNotFoundException
     * @throws UnprocessableEntityException
     */
    public function create(array $request): object
    {
        $folder = null;
        $folderData = [];
        $folderData['body']['user_id'] = $request['auth_user_id'];

        if (isset($request['body']['project_id'])) {
            $project = $this->projectRepository
                ->findOneById($request['body']['project_id']);

            if (is_null($project)) {
                throw new HttpNotFoundException('project_id does not exist');
            }

            $folder = $this->folderRepository
                ->create($request['body']);

            Permission::create(['name' => 'folders.*.' . $folder->id]);
            $request['user']->givePermissionTo('folders.*.' . $folder->id);

            $folderData['body']['folder_id'] = $folder->id;
            $folderData['body']['is_owner'] = true;

            if (isset($request['body']['settings'])) {
                $folderData['body']['settings'] = $request['body']['settings'];
            }

            if (isset($request['body']['is_bookmark'])) {
                $folderData['body']['is_bookmark'] = $request['body']['is_bookmark'];
            }

            $this->userFolderService
                ->create($folderData);

            $project->folders()->save($folder);
        }

        if (isset($request['body']['room_id'])) {
            $room = $this->roomRepository
                ->findOneById($request['body']['room_id']);

            if (is_null($room)) {
                throw new HttpNotFoundException('room_id does not exist');
            }

            $existingName = $this->checkIfFolderExists($request['body']);

            if ($existingName) {
                throw new UnprocessableEntityException('Folder name has already been taken', 422);
            }
            $folder = $this->folderRepository
                ->create($request['body']);

            Permission::create(['name' => 'folders.*.' . $folder->id]);
            $request['user']->givePermissionTo('folders.*.' . $folder->id);

            $folderData['body']['folder_id'] = $folder->id;
            $folderData['body']['is_owner'] = true;

            if (isset($request['body']['settings'])) {
                $folderData['body']['settings'] = $request['body']['settings'];
            }

            if (isset($request['body']['is_bookmark'])) {
                $folderData['body']['is_bookmark'] = $request['body']['is_bookmark'];
            }

            $this->userFolderService
                ->create($folderData);

            $room->folders()->save($folder);
        }

        if (isset($request['body']['folder_id'])) {
            $ParentFolder = $this->folderRepository
                ->findOneById($request['body']['folder_id']);

            if (is_null($ParentFolder)) {
                throw new HttpNotFoundException('folder_id does not exist');
            }

            $folder = $this->folderRepository
                ->create($request['body']);

            Permission::create(['name' => 'folders.*.' . $folder->id]);
            $request['user']->givePermissionTo('folders.*.' . $folder->id);

            $folderData['body']['folder_id'] = $folder->id;
            $folderData['body']['is_owner'] = true;

            if (isset($request['body']['settings'])) {
                $folderData['body']['settings'] = $request['body']['settings'];
            }

            if (isset($request['body']['is_bookmark'])) {
                $folderData['body']['is_bookmark'] = $request['body']['is_bookmark'];
            }

            $this->userFolderService
                ->create($folderData);

            $ParentFolder->childFolders()->save($folder);
        }

        if (
            !isset($request['body']['project_id'])
            && !isset($request['body']['room_id'])
            && !isset($request['body']['folder_id'])
        ) {
            $folder = $this->folderRepository
                ->create($request['body']);

            Permission::create(['name' => 'folders.*.' . $folder->id]);
            $request['user']->givePermissionTo('folders.*.' . $folder->id);

            if ($folder) {
                $folderData['body']['folder_id'] = $folder->id;
                $folderData['body']['is_owner'] = true;

                if (isset($request['body']['settings'])) {
                    $folderData['body']['settings'] = $request['body']['settings'];
                }

                if (isset($request['body']['is_bookmark'])) {
                    $folderData['body']['is_bookmark'] = $request['body']['is_bookmark'];
                }

                $this->userFolderService
                    ->create($folderData);
            }
        }
        return $folder;
    }

    /**
     * @param array $request
     * @param int $folderId
     * @return bool
     * @throws HttpNotFoundException
     * @throws UnprocessableEntityException
     * @throws PermissionDeniedException
     */
    public function update(array $request, int $folderId): bool
    {
        if ($request['user']->can('folders.*.' . $folderId)) {
            $folderData['body'] = [];
            $folder = $this->folderRepository->findOneById($folderId);
            if ($folder->entity_type == Room::class) {
                $existingName = $this->checkIfFolderExists(
                    ['name' => $request['body']['name'],
                        'room_id' => $folder->entity_id],
                    $folderId
                );
                if ($existingName) {
                    throw new UnprocessableEntityException('Folder name has already been taken', 422);
                }
            }

            if (isset($request['body']['folder_id'])) {
                if ($request['body']['folder_id'] == $folderId) {
                    throw new PermissionDeniedException('permission denied');
                }
                $folder = $this->folderRepository
                    ->findOneById($request['body']['folder_id']);

                if (is_null($folder)) {
                    throw new HttpNotFoundException('folder_id does not exist');
                }

                $request['body']['entity_id'] = $request['body']['folder_id'];
                $request['body']['entity_type'] = $folder->getMorphClass();
            }

            if (isset($request['body']['project_id'])) {
                $project = $this->projectRepository
                    ->findOneById($request['body']['project_id']);

                if (is_null($project)) {
                    throw new HttpNotFoundException('project_id does not exist');
                }

                $request['body']['entity_id'] = $request['body']['project_id'];
                $request['body']['entity_type'] = $project->getMorphClass();
            }

            if (isset($request['body']['room_id'])) {
                $room = $this->roomRepository
                    ->findOneById($request['body']['room_id']);

                if (is_null($room)) {
                    throw new HttpNotFoundException('room_id does not exist');
                }

                $request['body']['entity_id'] = $request['body']['room_id'];
                $request['body']['entity_type'] = $room->getMorphClass();
            }

            if (isset($request['body']['library']) && $request['body']['library'] === "true") {
                $request['body']['entity_id'] = null;
                $request['body']['entity_type'] = null;
            }


            $folderUpdated = $this->folderRepository
                ->update($request['body'], $folderId);

            if ($folderUpdated) {
                $folderData['body']['folder_id'] = $folderId;

                if (isset($request['body']['is_owner'])) {
                    $folderData['body']['is_owner'] = $request['body']['is_owner'];
                }

                if (isset($request['body']['is_bookmark'])) {
                    $folderData['body']['is_bookmark'] = $request['body']['is_bookmark'];
                }

                if (isset($request['body']['settings'])) {
                    $folderData['body']['settings'] = json_encode($request['body']['settings']);
                }

                return $this->userFolderRepository
                    ->newQuery()
                    ->where('user_id', '=', $request['auth_user_id'])
                    ->where('folder_id', '=', $folderId)
                    ->update($folderData['body']);
            }
        }
        return false;
    }

    /**
     * @param array $request
     * @param string $folderId
     * @return object|null
     * @throws FileNotFoundException
     * @throws HttpNotFoundException
     * @throws PermissionDeniedException
     */
    public function moveToBin(array $request, string $folderId): ?object
    {
        $accessFolderIds = [];

        foreach (explode(',', $folderId) as $id) {
            if ($request['user']->can('folders.*.' . $id)) {
                $accessFolderIds[] = $id;
            };
        }

        if (count(explode(',', $folderId)) > 1) {
            foreach (explode(',', $folderId) as $id) {
                if (!in_array($id, $accessFolderIds)) {
                    throw new PermissionDeniedException('permission denied');
                }
            }
        } else {
            if (!in_array($folderId, $accessFolderIds)) {
                throw new PermissionDeniedException('permission denied');
            }
        };

        $folder = $this->folderRepository
            ->addInBin($request, implode(',', $accessFolderIds));

        if (is_null($folder)) {
            throw new HttpNotFoundException('folder id does not exist');
        }

        return new stdClass();
    }


    /**
     * @param array $request
     * @param string $folderId
     *
     * @return bool
     * @throws FileNotFoundException
     */
    public function delete(array $request, string $folderId): bool
    {
        $accessFolderIds = explode(',', $folderId);

        foreach ($accessFolderIds as $id) {
            if (!$request['user']->can('folders.*.' . $id)) {
                return false;
            }
        }
        return $this->folderRepository->deleteManyFolders($accessFolderIds);
    }

    /**
     * @param array $request
     * @param int $folderId
     * @return AnonymousResourceCollection
     * @throws PermissionDeniedException
     */
    public function getFolderDataById(array $request, int $folderId): AnonymousResourceCollection
    {
        $userFolder = $this->userFolderRepository->newQuery()
            ->where('user_id', '=', $request['auth_user_id'])
            ->where('folder_id', '=', (int)$folderId)->first();
        if (is_null($userFolder)) {
            throw new PermissionDeniedException('permission denied', Response::HTTP_FORBIDDEN);
        }
        return FolderAndUserFilterResource::collection(
            $this->folderRepository
                ->getFolderDataById($request, $folderId)
        );
    }

    /**
     * @param array $queryOptions
     * @return AnonymousResourceCollection
     */
    public function getFolders(array $queryOptions): AnonymousResourceCollection
    {
        $queryOptions['User'] = $this->userRepository
            ->getModel();

        return FolderResource::collection(
            $this->folderRepository
                ->getFolders($queryOptions)
        );
    }

    /**
     * @param array $request
     *
     * @return object
     * @throws PermissionDeniedException
     */
    public function sendInvite(array $request): object
    {
        return $this->inviteService
            ->sendInvite($request, 'invite', Folder::class);
    }

    /**
     * @param array $queryOptions
     * @return array|null
     * @throws \App\Exceptions\Auth\AuthenticateException
     */
    public function acceptInvite(array $queryOptions): ?array
    {
        return $this->inviteService
            ->acceptInvite($queryOptions, $this->userFolderService);
    }

    /**
     * @param array $requestData
     *
     * @param int $folder_id
     * @return int
     */
    public function checkIfFolderExists(array $requestData, int $folder_id = 0): int
    {
        return $this->folderRepository->newQuery()
            ->where('name', $requestData['name'])
            ->where('entity_type', Room::class)
            ->where('entity_id', $requestData['room_id'])
            ->where('id', '!=', $folder_id)
            ->count();
    }
}
