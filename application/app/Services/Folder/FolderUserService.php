<?php

namespace App\Services\Folder;

use App\Interfaces\Invite\ISystemInviteEntity;
use App\Repositories\Folder\FolderUserRepository;
use Illuminate\Database\Eloquent\Model;

class FolderUserService implements ISystemInviteEntity
{
    /**
     * @var FolderUserRepository
     */
    private FolderUserRepository $folderUserRepository;

    /**
     * ProjectService constructor.
     *
     * @param FolderUserRepository $folderUserRepository
     */
    public function __construct(FolderUserRepository $folderUserRepository)
    {
        $this->folderUserRepository = $folderUserRepository;
    }

    /**
     * @param array $options
     *
     * @param array $data
     * @return Model
     */
    public function updateOrCreate(array $options, array $data): Model
    {
        return $this->folderUserRepository
            ->updateOrCreate($options, $data['body']);
    }
}
