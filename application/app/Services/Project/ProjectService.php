<?php

namespace App\Services\Project;

use App\Entity\ProjectPackages\Project;
use App\Exceptions\Common\HttpNotFoundException;
use App\Exceptions\Common\PermissionDeniedException;
use App\Repositories\Project\ProjectRepository;
use App\Repositories\User\UserProjectRepository;
use App\Repositories\User\UserRepository;
use App\Resources\Project\ProjectMembersResource;
use App\Resources\Project\ProjectResource;
use App\Resources\Project\ProjectsResource;
use App\Services\AbstractService;
use App\Services\Invite\InviteService;
use App\Services\User\UserProjectService;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Spatie\Permission\Models\Permission;
use stdClass;
use Symfony\Component\HttpFoundation\Response;

class ProjectService extends AbstractService
{
    /**
     * @var ProjectRepository
     * @var UserProjectService
     * @var UserProjectRepository
     * @var UserRepository
     * @var InviteService
     */
    private ProjectRepository $projectRepository;
    private UserProjectService $userProjectService;
    private UserProjectRepository $userProjectRepository;
    private UserRepository $userRepository;
    private InviteService $inviteService;

    /**
     * ProjectService constructor.
     *
     * @param ProjectRepository $projectRepository
     * @param UserProjectService $userProjectService
     * @param UserProjectRepository $userProjectRepository
     * @param UserRepository $userRepository
     * @param InviteService $inviteService
     */
    public function __construct(
        ProjectRepository     $projectRepository,
        UserProjectService    $userProjectService,
        UserProjectRepository $userProjectRepository,
        UserRepository        $userRepository,
        InviteService         $inviteService
    )
    {
        $this->projectRepository = $projectRepository;
        $this->userProjectService = $userProjectService;
        $this->userProjectRepository = $userProjectRepository;
        $this->userRepository = $userRepository;
        $this->inviteService = $inviteService;
    }

    /**
     * @param array $request
     * @return array
     */
    public function create(array $request): array
    {
        $projectData = [];
        $projectData['body']['user_id'] = $request['auth_user_id'];

        $project = $this->projectRepository
            ->create($request['body']);

        Permission::create(['name' => 'projects.owner.' . $project->id]);
        $request['user']->givePermissionTo('projects.owner.' . $project->id);

        if ($project) {
            $projectData['body']['project_id'] = $project->id;
            $projectData['body']['is_owner'] = true;
            $projectData['body']['role_name'] = 'Owner';

            if (isset($request['body']['settings'])) {
                $projectData['body']['settings'] = $request['body']['settings'];
            } else {
                $projectData['body']['settings'] = [];
            }

            if (isset($request['body']['is_bookmark'])) {
                $projectData['body']['is_bookmark'] = $request['body']['is_bookmark'];
            } else {
                $projectData['body']['is_bookmark'] = false;
            }

            $projectRelationData = $this->userProjectService
                ->create($projectData);

            return array_merge($project->toArray(), $projectRelationData->toArray());
        }
    }

    /**
     * @param array $request
     *
     * @param int $projectId
     * @return bool
     * @throws PermissionDeniedException
     */
    public function update(array $request, int $projectId): bool
    {
        if ($request['user']->can('projects.owner.' . $projectId)) {
            $projectData['body'] = [];
            if ($this->checkIfProjectExists($projectId, $request['body'])) {
                throw new PermissionDeniedException("Project {$request['body']['name']} already exists");
            }
            $project = $this->projectRepository
                ->update($request['body'], $projectId);

            if ($project) {
                $projectData['body']['project_id'] = $projectId;

                if (isset($request['body']['is_owner'])) {
                    $projectData['body']['is_owner'] = $request['body']['is_owner'];
                }

                if (isset($request['body']['settings'])) {
                    $projectData['body']['settings'] = json_encode($request['body']['settings']);
                }

                if (isset($request['body']['is_bookmark'])) {
                    $projectData['body']['is_bookmark'] = $request['body']['is_bookmark'];
                }

                return $this->userProjectRepository
                    ->newQuery()
                    ->where('user_id', '=', $request['auth_user_id'])
                    ->where('project_id', '=', $projectId)
                    ->update($projectData['body']);
            }
        }

        throw new PermissionDeniedException('Нет доступа к проекту.');
    }

    /**
     * @param array $request
     * @param string $projectId
     * @return object|null
     * @throws FileNotFoundException
     * @throws HttpNotFoundException
     * @throws PermissionDeniedException
     */
    public function moveToBin(array $request, string $projectId): ?object
    {
        $accessProjectIds = [];

        foreach (explode(',', $projectId) as $id) {
            if ($request['user']->can('projects.owner.' . $id) || $request['user']->can('projects.Manager.' . $id)) {
                $accessProjectIds[] = $id;
            };
        }

        if (count(explode(',', $projectId)) > 1) {
            foreach (explode(',', $projectId) as $id) {
                if (!in_array($id, $accessProjectIds)) {
                    throw new PermissionDeniedException('permission denied');
                }
            }
        } else {
            if (!in_array($projectId, $accessProjectIds)) {
                throw new PermissionDeniedException('permission denied');
            }
        };

        $project = $this->projectRepository
            ->addInBin($request, implode(',', $accessProjectIds));

        if (is_null($project)) {
            throw new HttpNotFoundException('project id does not exist');
        }

        return new stdClass();
    }

    /**
     * @param array $request
     * @param string $projectId
     * @return bool
     * @throws FileNotFoundException
     */
    public function delete(array $request, string $projectId): bool
    {
        $accessProjectIds = [];

        foreach (explode(',', $projectId) as $id) {
            if ($request['user']->can('projects.owner.' . $id) || $request['user']->can('projects.Manager.' . $id)) {
                $accessProjectIds[] = $id;
            };
        }
        $projectIds = (explode(',', $projectId));
        foreach ($projectIds as $projId) {
            if (!in_array($projId, $accessProjectIds)) {
                return false;
            }
        }

        return $this->projectRepository
            ->deleteManyProjects($accessProjectIds);
    }

    /**
     * @param array $request
     * @param int $projectId
     *
     * @return AnonymousResourceCollection
     * @throws HttpNotFoundException
     * @throws PermissionDeniedException
     */
    public function getProjectDataById(array $request, int $projectId)
    {
        $userProject = $this->userProjectRepository->newQuery()
            ->where('user_id', '=', $request['auth_user_id'])
            ->where('project_id', '=', $projectId)->first();

        if (is_null($userProject)) {
            throw new PermissionDeniedException('permission denied', Response::HTTP_FORBIDDEN);
        }
        $data = $this->projectRepository
            ->getProjectDataById($request, $projectId);

        if (!empty($data->toArray())) {
            return ProjectResource::collection($data);
        }

        throw new HttpNotFoundException('project not found');
    }

    /**
     * @param array $queryOptions
     * @return AnonymousResourceCollection
     */
    public function getProjects(array $queryOptions): AnonymousResourceCollection
    {
        $queryOptions['User'] = $this->userRepository
            ->getModel();

        return ProjectsResource::collection(
            $this->projectRepository
                ->getProjects($queryOptions)
        );
    }

    /**
     * @param array $queryOptions
     * @param $projectId
     * @return AnonymousResourceCollection
     */
    public function getProjectMembers(array $queryOptions, $projectId)
    {
        $project = $this->projectRepository->getProjectMembers($queryOptions, $projectId);
        return ProjectMembersResource::collection(
            $project && count($project->users) ? $project->users : []
        );
    }

    /**
     * @param array $request
     *
     * @return object
     * @throws PermissionDeniedException
     */
    public function sendInvite(array $request): object
    {
        if (
            $request['user']->can('projects.owner.' . $request['body']['entity_id'])
            ||
            $request['user']->can('projects.Manager.' . $request['body']['entity_id'])
        ) {
            return $this->inviteService
                ->sendInvite($request, 'invite', Project::class);
        };
        throw new PermissionDeniedException('permission denied', Response::HTTP_FORBIDDEN);
    }

    /**
     * @param array $queryOptions
     *
     * @return array|null
     * @throws \App\Exceptions\Auth\AuthenticateException
     */
    public function acceptInvite(array $queryOptions): ?array
    {
        return $this->inviteService
            ->acceptInvite($queryOptions, $this->userProjectService);
    }

    /**
     * @param int $projectId
     * @param array $data
     * @return int
     */
    public function checkIfProjectExists(int $projectId, array $data): int
    {
        return $this->projectRepository->newQuery()->where('name', $data['name'])->where('id', '!=', $projectId)->count();
    }
}
