<?php

namespace App\Entity\FolderPackages;

use App\Entity\AuthTrait;
use App\Entity\DocumentPackages\Document;
use App\Entity\UserPackages\UserFolder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use ShiftOneLabs\LaravelCascadeDeletes\CascadesDeletes;

class Folder extends Model
{
    use CascadesDeletes;
    use HasFactory;
    use AuthTrait;

    /**
     * @var string
     */
    protected $guard_name = 'api';

    /**
     * @var array
     */
    protected $cascadeDeletes = ['documents'];

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'image',
        'entity_id',
        'entity_type',
        'deleted_at'
    ];

    /**
     * @return MorphTo
     */
    public function entity(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return MorphMany
     */
    public function documents(): MorphMany
    {
        return $this->morphMany(Document::class, 'entity');
    }

    /**
     * @return MorphMany
     */
    public function notDeletedDocuments(): MorphMany
    {
        return $this->morphMany(Document::class, 'entity')
            ->whereNull('deleted_at');
    }

    /**
     * @return MorphMany
     */
    public function notDeletedChildFolders(): MorphMany
    {
        return $this->morphMany(Folder::class, 'entity')
            ->whereNull('deleted_at');
    }

    /**
     * @return MorphMany
     */

    public function folderDocuments()
    {
        return $this->notDeletedDocuments();
    }

    /**
     * @return MorphOne
     */
    public function deletedDocument(): MorphOne
    {
        return $this->morphOne(Document::class, 'entity')
            ->whereNotNull('deleted_at');
    }

    /**
     * @return HasOne
     */
    public function folder(): HasOne
    {
        return $this->hasOne(UserFolder::class, 'folder_id');
    }

    /**
     * @return MorphMany
     */

    public function childFolders(): MorphMany
    {
        return $this->morphMany(Folder::class, 'entity');
    }
}
