<?php

namespace App\Entity\ProjectPackages;

use App\Entity\AuthTrait;
use App\Entity\DocumentPackages\Document;
use App\Entity\FolderPackages\Folder;
use App\Entity\RoomPackages\Room;
use App\Entity\UserPackages\User;
use App\Entity\UserPackages\UserProject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use ShiftOneLabs\LaravelCascadeDeletes\CascadesDeletes;

class Project extends Model
{
    use CascadesDeletes;
    use HasFactory;
    use AuthTrait;

    /**
     * @var string
     */
    protected $guard_name = 'api';

    /**
     * @var array
     */
    protected $cascadeDeletes = ['folders', 'documents', 'rooms'];

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'image',
        'icon',
        'color',
        'deleted_at'
    ];

    /**
     * @return MorphMany
     */
    public function folders(): MorphMany
    {
        return $this->morphMany(Folder::class, 'entity');
    }

    /**
     * @return HasMany
     */
    public function rooms(): HasMany
    {
        return $this->hasMany(Room::class, 'project_id', 'id');
    }

    /**
     * @return MorphMany
     */
    public function documents(): MorphMany
    {
        return $this->morphMany(Document::class, 'entity');
    }

    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_projects', 'project_id');
    }
}
