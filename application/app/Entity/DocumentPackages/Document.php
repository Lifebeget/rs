<?php

namespace App\Entity\DocumentPackages;

use App\Entity\AuthTrait;
use App\Entity\FolderPackages\Folder;
use App\Entity\UserPackages\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use ShiftOneLabs\LaravelCascadeDeletes\CascadesDeletes;

class Document extends Model
{
    use CascadesDeletes;
    use HasFactory;
    use AuthTrait;

    /**
     * @var string
     */
    protected $guard_name = 'api';

    /**
     * @var array
     */
    protected $cascadeDeletes = ['folders'];

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'image',
        'entity_id',
        'entity_type',
        'version',
        'deleted_at'
    ];

    /**
     * @return MorphTo
     */
    public function entity(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return MorphMany
     */
    public function folders(): MorphMany
    {
        return $this->morphMany(Folder::class, 'entity');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_documents');
    }
}
