<?php

namespace App\Entity\DocumentPackages;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentInviteLink extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $fillable = [
        'user_email',
        'token',
        'entity_id',
        'settings'
    ];

    protected $casts = [
        'settings' => 'array'
    ];
}
