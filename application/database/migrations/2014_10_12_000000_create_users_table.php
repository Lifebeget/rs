<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->nullable();
            $table->string('surname')->nullable();
            $table->text('image')->nullable();
            $table->string('email')->index()->unique();
            $table->string('email_code')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('email_password')->nullable();
            $table->string('login_password')->nullable();
            $table->jsonb('tariff')->default('[]');
            $table->string('color')->nullable();
            $table->string('country')->nullable();
            $table->boolean('blocked')->default(0);
            $table->rememberToken();
            $table->timestamps();
            $table->index(['created_at', 'updated_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users', function (Blueprint $table) {
            $table->dropIndex(['created_at', 'updated_at']);
        });
    }
}
