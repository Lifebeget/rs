<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentInviteLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_invite_links', function (Blueprint $table) {
            $table->id();
            $table->text('token');
            $table->unsignedBigInteger('entity_id');
            $table->string('user_email');
            $table->timestamps();
            $table->index(['created_at', 'entity_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_invite_links');
    }
}
