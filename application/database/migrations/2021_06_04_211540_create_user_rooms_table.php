<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_rooms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete();
            $table->unsignedBigInteger('room_id')->index();
            $table->foreign('room_id')->references('id')->on('rooms')->cascadeOnDelete();
            $table->boolean('is_owner')->default(0);
            $table->boolean('is_bookmark')->default(0);
            $table->jsonb('settings')->default('[]');
            $table->softDeletes();
            $table->index(['created_at', 'updated_at', 'deleted_at']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_rooms', function (Blueprint $table) {
            $table->dropForeign(['user_id', 'room_id']);
            $table->dropColumn('user_id', 'room_id');
            $table->dropIndex([
                'user_id',
                'room_id',
                'deleted_at',
                'created_at',
                'updated_at'
            ]);
        });
    }
}
