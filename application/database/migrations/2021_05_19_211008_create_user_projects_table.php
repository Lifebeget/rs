<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_projects', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete();
            $table->unsignedBigInteger('project_id')->index();
            $table->foreign('project_id')->references('id')->on('projects')->cascadeOnDelete();
            $table->boolean('is_owner')->default(0);
            $table->boolean('is_bookmark')->default(0);
            $table->jsonb('settings')->default('[]');
            $table->softDeletes();
            $table->index(['created_at', 'updated_at', 'deleted_at']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_projects', function (Blueprint $table) {
            $table->dropForeign(['user_id', 'project_id']);
            $table->dropColumn('user_id', 'project_id');
            $table->dropIndex([
                'user_id',
                'project_id',
                'deleted_at',
                'created_at',
                'updated_at'
            ]);
        });
    }
}
