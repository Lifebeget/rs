<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_documents', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete();
            $table->unsignedBigInteger('document_id')->index();
            $table->foreign('document_id')->references('id')->on('documents')->cascadeOnDelete();
            $table->boolean('is_owner')->default(0);
            $table->boolean('is_bookmark')->default(0);
            $table->jsonb('settings')->nullable();
            $table->softDeletes();
            $table->index(['created_at', 'updated_at', 'deleted_at']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_documents', function (Blueprint $table) {
            $table->dropForeign(['user_id', 'document_id']);
            $table->dropColumn('user_id', 'document_id');
            $table->dropIndex([
                'user_id',
                'document_id',
                'deleted_at',
                'created_at',
                'updated_at'
            ]);
        });
    }
}
