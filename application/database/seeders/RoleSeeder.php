<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Exception
     */
    public function run()
    {
        $roles = [
            ['name' => 'admin', 'guard_name' => 'api'],
            ['name' => 'owner', 'guard_name' => 'api'],
            ['name' => 'manager', 'guard_name' => 'api'],
            ['name' => 'member', 'guard_name' => 'api'],
        ];

        $createdRole = Role::insert($roles);

        if ($createdRole) {
            $permissions = [
                ['name' => 'create documents', 'guard_name' => 'api'],
                ['name' => 'delete documents', 'guard_name' => 'api'],
                ['name' => 'edit documents', 'guard_name' => 'api'],
                ['name' => 'send documents invite', 'guard_name' => 'api'],
                ['name' => 'documents.*', 'guard_name' => 'api'],
                ['name' => 'create folders', 'guard_name' => 'api'],
                ['name' => 'delete folders', 'guard_name' => 'api'],
                ['name' => 'edit folders', 'guard_name' => 'api'],
                ['name' => 'send folders invite', 'guard_name' => 'api'],
                ['name' => 'folders.*', 'guard_name' => 'api'],
                ['name' => 'create projects', 'guard_name' => 'api'],
                ['name' => 'delete projects', 'guard_name' => 'api'],
                ['name' => 'edit projects', 'guard_name' => 'api'],
                ['name' => 'send projects invite', 'guard_name' => 'api'],
                ['name' => 'projects.*', 'guard_name' => 'api'],
                ['name' => 'rooms.*', 'guard_name' => 'api'],
                ['name' => 'create rooms', 'guard_name' => 'api'],
                ['name' => 'delete rooms', 'guard_name' => 'api'],
                ['name' => 'edit rooms', 'guard_name' => 'api'],
                ['name' => 'send rooms invite', 'guard_name' => 'api'],
            ];
            Permission::insert($permissions);
        }
    }
}
